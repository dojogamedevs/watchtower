{
    "id": "fb24ce73-cf76-453a-8551-08119588dd56",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_b_shockwave",
    "eventList": [
        {
            "id": "155951ac-b62f-471c-9b17-86eeea8f3f3f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fb24ce73-cf76-453a-8551-08119588dd56"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f80786cf-f608-43c1-9cae-6ef18095a251",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "26aa48d9-7207-4a55-9f16-80db2f371f40",
    "visible": true
}