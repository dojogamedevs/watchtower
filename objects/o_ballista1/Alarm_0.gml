/// @description Insert description here
// You can write your code in this editor
event_inherited();

if level >= 3 {
	if cupid_ct < 2 {
		alarm[0] = 5;
		cupid_ct += 1;
	} else {
		cupid_ct = 0;
		alarm[0] = room_speed / fire_rate;
	}
}