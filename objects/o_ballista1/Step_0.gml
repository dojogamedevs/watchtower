/// @description Insert description here
// You can write your code in this editor
event_inherited();
if level == 1 {
	damage = 20;
	range = 100;
	fire_rate = 2.5;
	cost = 1;
} else if level == 2 {
	damage = 35;
	range = 125;
	fire_rate = 3.0;
	cost = 1;
} else if level >= 3 {
	damage = 35;
	range = 125;
	fire_rate = 3.5;
	cost = 2;
}	