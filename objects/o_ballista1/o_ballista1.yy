{
    "id": "8a768db1-1ab4-4ecd-9cac-a3d2be6e5e6f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_ballista1",
    "eventList": [
        {
            "id": "4539b16b-9e49-4e0e-8d72-25c7887c3e1a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8a768db1-1ab4-4ecd-9cac-a3d2be6e5e6f"
        },
        {
            "id": "ca93df9c-73b4-414b-be47-36ea8e9045c0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "8a768db1-1ab4-4ecd-9cac-a3d2be6e5e6f"
        },
        {
            "id": "4007a57b-0764-4ee2-b1e3-2ae9f13c8308",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "8a768db1-1ab4-4ecd-9cac-a3d2be6e5e6f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "75ee5a79-72c4-4a2c-b567-32cea83bd138",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "539e3bcf-2764-4c61-84b6-6b377b9b6bca",
    "visible": true
}