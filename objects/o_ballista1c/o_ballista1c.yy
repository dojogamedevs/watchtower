{
    "id": "fa7f12aa-7aae-4fcc-8711-baef5c2027d5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_ballista1c",
    "eventList": [
        {
            "id": "d9378749-1127-4c21-9889-8f548e15fbe0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fa7f12aa-7aae-4fcc-8711-baef5c2027d5"
        },
        {
            "id": "dd0fb39a-7a45-423f-b73c-f971db96be9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "fa7f12aa-7aae-4fcc-8711-baef5c2027d5"
        },
        {
            "id": "0d044167-c523-4263-863c-f4ea862ea251",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "fa7f12aa-7aae-4fcc-8711-baef5c2027d5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "539e3bcf-2764-4c61-84b6-6b377b9b6bca",
    "visible": true
}