{
    "id": "3eeb192a-5fb4-411c-a950-6286bf438405",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_ballista1d",
    "eventList": [
        {
            "id": "8812bbf9-d245-4bce-b465-812432d1c918",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3eeb192a-5fb4-411c-a950-6286bf438405"
        },
        {
            "id": "e12a5c55-9c08-41aa-b2fb-b0860a171901",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3eeb192a-5fb4-411c-a950-6286bf438405"
        },
        {
            "id": "c77c6c5a-bd2c-413c-98a2-8e3b2c4dfddf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "3eeb192a-5fb4-411c-a950-6286bf438405"
        },
        {
            "id": "7019a163-e575-4282-84b6-749261afe71f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "3eeb192a-5fb4-411c-a950-6286bf438405"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "539e3bcf-2764-4c61-84b6-6b377b9b6bca",
    "visible": true
}