{
    "id": "7f127764-d503-48d8-81d5-c5a0205a5277",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_bullet",
    "eventList": [
        {
            "id": "13c731a1-3607-4d42-bd97-1c7453171e25",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "7f127764-d503-48d8-81d5-c5a0205a5277"
        },
        {
            "id": "932db9e6-25d2-499e-a626-120aa58793dc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7f127764-d503-48d8-81d5-c5a0205a5277"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f80786cf-f608-43c1-9cae-6ef18095a251",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "363eb404-57a9-4523-a264-368b3f61be64",
    "visible": true
}