{
    "id": "73f67389-de39-448f-87f1-3aa42b7b4a93",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_ballista",
    "eventList": [
        {
            "id": "56a5fcd4-01b7-4e0c-b1c8-6336c98a8b6d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "73f67389-de39-448f-87f1-3aa42b7b4a93"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b9d693a8-d11c-4cc4-86cf-2ccad033873c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "59c0f3bb-3b49-41ec-930f-2da9b48d6371",
    "visible": true
}