{
    "id": "2268c125-ca1d-4cdf-90a1-9dfecbbaafc1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_catapult",
    "eventList": [
        {
            "id": "178d106b-e806-4dfc-8832-bbf833c2cfab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "2268c125-ca1d-4cdf-90a1-9dfecbbaafc1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b9d693a8-d11c-4cc4-86cf-2ccad033873c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "98f6edd4-24e5-4037-ae71-7bceb8610b11",
    "visible": true
}