/// @description Insert description here
// You can write your code in this editor
event_inherited();

if state == cardstate.placed {
	var draw_effect = instance_create_depth(0, 0, 0, obj_effect_draw);
	draw_effect.amount = 1;
	draw_effect.next_state = effectstate.active;
}