{
    "id": "36916a3d-40e9-4e36-bda9-7b4e4d06f3e4",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_outpost",
    "eventList": [
        {
            "id": "29e7ba6d-6682-4f37-8781-ec07d98c6473",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "36916a3d-40e9-4e36-bda9-7b4e4d06f3e4"
        },
        {
            "id": "66af3d80-8144-4687-8d79-379052039453",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "36916a3d-40e9-4e36-bda9-7b4e4d06f3e4"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b9d693a8-d11c-4cc4-86cf-2ccad033873c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c77b6449-d179-4149-8c60-207effa2dbd1",
    "visible": true
}