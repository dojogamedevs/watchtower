{
    "id": "9adbc604-a31b-4301-990f-f44e2696bfb7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_pierce",
    "eventList": [
        {
            "id": "9dd57ced-10d0-4998-95d8-46fb1d95945d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9adbc604-a31b-4301-990f-f44e2696bfb7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b9d693a8-d11c-4cc4-86cf-2ccad033873c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "402faf73-e25a-4086-914d-3252bd37db03",
    "visible": true
}