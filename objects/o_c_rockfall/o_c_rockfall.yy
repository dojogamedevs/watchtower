{
    "id": "9ce7a26a-0dc6-409b-b998-d16ad97a4a35",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_rockfall",
    "eventList": [
        {
            "id": "ba64ff34-5398-4979-929b-5f1fe5f95dba",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9ce7a26a-0dc6-409b-b998-d16ad97a4a35"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b9d693a8-d11c-4cc4-86cf-2ccad033873c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "636b2577-5959-492f-9e35-f8f81b42b7fe",
    "visible": true
}