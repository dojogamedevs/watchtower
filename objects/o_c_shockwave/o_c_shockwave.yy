{
    "id": "1b5063cd-fd8c-4de5-9b65-a7ed325bc314",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_shockwave",
    "eventList": [
        {
            "id": "088e2af9-692c-41ff-9b5e-cbe478795601",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1b5063cd-fd8c-4de5-9b65-a7ed325bc314"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b9d693a8-d11c-4cc4-86cf-2ccad033873c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "ccd96987-5a4b-4c00-ac06-4b56db76b06b",
    "visible": true
}