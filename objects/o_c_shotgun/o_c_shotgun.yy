{
    "id": "5aafbf50-5125-4001-9550-f6901a8e0e9e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_shotgun",
    "eventList": [
        {
            "id": "625c50a3-259f-43c9-af70-b9986b9df7c2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "5aafbf50-5125-4001-9550-f6901a8e0e9e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b9d693a8-d11c-4cc4-86cf-2ccad033873c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "9baaa103-26ac-4c0e-ae01-78539653529f",
    "visible": true
}