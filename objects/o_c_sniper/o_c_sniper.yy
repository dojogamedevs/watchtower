{
    "id": "d342b1d4-b19f-489a-8c6d-3091e5dbe6f1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_sniper",
    "eventList": [
        {
            "id": "7aec5bb7-86f8-44ed-88bc-54cbd923d388",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d342b1d4-b19f-489a-8c6d-3091e5dbe6f1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b9d693a8-d11c-4cc4-86cf-2ccad033873c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "38384fcb-e875-4e90-9484-bae986ce3c42",
    "visible": true
}