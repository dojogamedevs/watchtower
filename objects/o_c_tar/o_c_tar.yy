{
    "id": "376b48d3-79a8-4ffa-abcd-51880b99fa61",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_c_tar",
    "eventList": [
        {
            "id": "95e30cdd-19a5-408e-ac4c-2872fc4d6222",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "376b48d3-79a8-4ffa-abcd-51880b99fa61"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "b9d693a8-d11c-4cc4-86cf-2ccad033873c",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "f2b13367-6515-4cad-a439-5a74d6279bb8",
    "visible": true
}