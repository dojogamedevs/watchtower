{
    "id": "8a5a3fff-7bb5-4c5c-b542-2a19b0da81d9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_catab",
    "eventList": [
        {
            "id": "a822e211-0aa1-462a-8475-51a2901f53bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "8a5a3fff-7bb5-4c5c-b542-2a19b0da81d9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f80786cf-f608-43c1-9cae-6ef18095a251",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "155b0836-436f-429f-bfbe-f4589372550e",
    "visible": true
}