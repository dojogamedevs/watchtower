{
    "id": "aabf670b-d259-4272-882a-62ad212f7c3e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_catapult1",
    "eventList": [
        {
            "id": "48a924a4-88a0-4cc4-9276-3a2fac83e30e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "aabf670b-d259-4272-882a-62ad212f7c3e"
        },
        {
            "id": "0f08da5c-8d21-468c-974c-8f2dc183714c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "aabf670b-d259-4272-882a-62ad212f7c3e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "75ee5a79-72c4-4a2c-b567-32cea83bd138",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "58c8c2bf-7933-4a7b-8a61-1d651a89d487",
    "visible": true
}