{
    "id": "3d110813-556d-4bb4-bd3c-20203b076853",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_deadenemy",
    "eventList": [
        {
            "id": "358d73f5-dccf-4ffd-8d19-31ec89bdd679",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3d110813-556d-4bb4-bd3c-20203b076853"
        },
        {
            "id": "11c21fe3-816b-4c80-b91c-724a82563a88",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3d110813-556d-4bb4-bd3c-20203b076853"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "bde22c92-93cd-47fc-aae7-6d7d21ef859c",
    "visible": true
}