/// @description initialize

randomize();
hand_size = 5
deck_p = ds_list_create();
scr_create_deck();

current_player = instance_find(obj_player, 0)
current_player.current_deck = deck_p;

hand_p = ds_list_create();
//starting hand
draw(hand_size);

deck_size = 0;