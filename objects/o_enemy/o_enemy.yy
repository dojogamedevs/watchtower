{
    "id": "bbebf1e1-d28c-4719-84bf-7c649349b2af",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_enemy",
    "eventList": [
        {
            "id": "6ed52340-8aba-4d0f-bba2-b028b712ac55",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bbebf1e1-d28c-4719-84bf-7c649349b2af"
        },
        {
            "id": "7c61caa2-887d-44ee-ba39-8fc629b37b1f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7f127764-d503-48d8-81d5-c5a0205a5277",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bbebf1e1-d28c-4719-84bf-7c649349b2af"
        },
        {
            "id": "0c806d29-8110-470f-a36e-3757da044bce",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1969cdd7-c1c8-48ad-a36b-b631d178e2bd",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bbebf1e1-d28c-4719-84bf-7c649349b2af"
        },
        {
            "id": "f9beb41a-9a97-4224-8d19-6f08be4175d1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bbebf1e1-d28c-4719-84bf-7c649349b2af"
        },
        {
            "id": "c650d318-aaf9-45a7-956d-ea16b3e1633a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7b3875be-5546-453e-95c6-7897b963a944",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bbebf1e1-d28c-4719-84bf-7c649349b2af"
        },
        {
            "id": "56a2e57b-8ba5-41e0-99bc-7287555bd32a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "bbebf1e1-d28c-4719-84bf-7c649349b2af"
        },
        {
            "id": "3e16f8bf-429b-48ba-a16e-03c41326a288",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 7,
            "m_owner": "bbebf1e1-d28c-4719-84bf-7c649349b2af"
        },
        {
            "id": "e7ec51fd-a238-405f-b0de-d1a99c0364fb",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "bbebf1e1-d28c-4719-84bf-7c649349b2af"
        },
        {
            "id": "15549038-2377-446c-989e-54ceea88a883",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d07b384d-71d3-4f01-9e37-75e1a6668133",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bbebf1e1-d28c-4719-84bf-7c649349b2af"
        },
        {
            "id": "4327cd8d-1f37-4bf1-a908-2d7b7a2af545",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "bbebf1e1-d28c-4719-84bf-7c649349b2af"
        },
        {
            "id": "eaf8c513-af02-4fc8-9a13-c81ffd72e27d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4cbe4217-cb70-4b67-a6b1-1d9c30e56f1a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bbebf1e1-d28c-4719-84bf-7c649349b2af"
        },
        {
            "id": "a8d7022f-6d6f-47a8-82f2-652daab21846",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "10f92d6b-f7d1-47e9-ab49-d257ab1a739a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bbebf1e1-d28c-4719-84bf-7c649349b2af"
        },
        {
            "id": "4294268f-fed7-4f01-90a2-5bb52930acbd",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8a5a3fff-7bb5-4c5c-b542-2a19b0da81d9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bbebf1e1-d28c-4719-84bf-7c649349b2af"
        },
        {
            "id": "22d52510-fbac-4541-a9bb-716b107d6e95",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "fb24ce73-cf76-453a-8551-08119588dd56",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "bbebf1e1-d28c-4719-84bf-7c649349b2af"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "756dc221-bc20-4fcd-95c4-65f99b5ee88f",
    "visible": true
}