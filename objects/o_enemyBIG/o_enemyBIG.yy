{
    "id": "bf3b2ef7-60ee-43f2-8fa6-62d567e7d838",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_enemyBIG",
    "eventList": [
        {
            "id": "caef7f6f-0c90-4a01-bc46-ef453b32765c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "bf3b2ef7-60ee-43f2-8fa6-62d567e7d838"
        },
        {
            "id": "0742f62c-692c-4d29-9d17-89e1bf97eca2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "bf3b2ef7-60ee-43f2-8fa6-62d567e7d838"
        },
        {
            "id": "8465e361-e86e-4cad-ab45-154a208c4f86",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 7,
            "m_owner": "bf3b2ef7-60ee-43f2-8fa6-62d567e7d838"
        },
        {
            "id": "5b2f22bf-8550-44a0-a3c9-149464390a2d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "bf3b2ef7-60ee-43f2-8fa6-62d567e7d838"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bbebf1e1-d28c-4719-84bf-7c649349b2af",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "0e9e403a-922c-4345-a3ae-15e2bc184c49",
    "visible": true
}