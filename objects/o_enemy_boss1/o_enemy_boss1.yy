{
    "id": "034b759c-ff5e-4d91-91bb-35d7cee780c2",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_enemy_boss1",
    "eventList": [
        {
            "id": "f1c05c6a-1d12-42ef-88f7-3a2776958199",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "034b759c-ff5e-4d91-91bb-35d7cee780c2"
        },
        {
            "id": "98a4afd9-786f-4a20-abe8-ff64a6a0b539",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "034b759c-ff5e-4d91-91bb-35d7cee780c2"
        },
        {
            "id": "216941e1-8886-4f59-9a72-09e984c65771",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 7,
            "m_owner": "034b759c-ff5e-4d91-91bb-35d7cee780c2"
        },
        {
            "id": "58af7145-4f90-4167-ade2-b1b0aec21d87",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "034b759c-ff5e-4d91-91bb-35d7cee780c2"
        },
        {
            "id": "511a1ec4-fb0e-4ce8-9281-760e099b61d9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "034b759c-ff5e-4d91-91bb-35d7cee780c2"
        },
        {
            "id": "e6f47cc4-b0e6-41b2-927d-624e37bb4cd4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 3,
            "eventtype": 2,
            "m_owner": "034b759c-ff5e-4d91-91bb-35d7cee780c2"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bbebf1e1-d28c-4719-84bf-7c649349b2af",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "79019127-0eab-4b64-87f6-6c54f5598e5d",
    "visible": true
}