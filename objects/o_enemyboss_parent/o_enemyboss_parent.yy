{
    "id": "685ec6f2-ebab-485a-9e12-b38b27898499",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_enemyboss_parent",
    "eventList": [
        {
            "id": "1656d493-3a38-49c8-9dc7-66d62fb31583",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "685ec6f2-ebab-485a-9e12-b38b27898499"
        },
        {
            "id": "c8a0dc81-1d64-4e97-9763-21f3f4dfae04",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7f127764-d503-48d8-81d5-c5a0205a5277",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "685ec6f2-ebab-485a-9e12-b38b27898499"
        },
        {
            "id": "3ec67a06-2ecb-4a42-9df4-d950dca364d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "1969cdd7-c1c8-48ad-a36b-b631d178e2bd",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "685ec6f2-ebab-485a-9e12-b38b27898499"
        },
        {
            "id": "cace11ea-0fa0-4b75-9f4c-495b9272dfc9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "685ec6f2-ebab-485a-9e12-b38b27898499"
        },
        {
            "id": "56794f1c-4710-433e-a383-402b592f399e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "7b3875be-5546-453e-95c6-7897b963a944",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "685ec6f2-ebab-485a-9e12-b38b27898499"
        },
        {
            "id": "20ba0039-02c8-4312-b6c0-3069899a60f9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "685ec6f2-ebab-485a-9e12-b38b27898499"
        },
        {
            "id": "25b0aaf2-ecfa-4aa0-bf1a-31f2ecaae3c7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 7,
            "m_owner": "685ec6f2-ebab-485a-9e12-b38b27898499"
        },
        {
            "id": "8c52d688-9420-4c56-9ba9-1d83a5a17a57",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "685ec6f2-ebab-485a-9e12-b38b27898499"
        },
        {
            "id": "52083d7b-e4ed-47d1-882c-d31c53380986",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "d07b384d-71d3-4f01-9e37-75e1a6668133",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "685ec6f2-ebab-485a-9e12-b38b27898499"
        },
        {
            "id": "edfc2eb6-3c03-4b0f-b999-dd6c1fda9855",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "685ec6f2-ebab-485a-9e12-b38b27898499"
        },
        {
            "id": "ad33e240-1a0e-4d84-85df-11e79fb2e111",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "4cbe4217-cb70-4b67-a6b1-1d9c30e56f1a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "685ec6f2-ebab-485a-9e12-b38b27898499"
        },
        {
            "id": "ffe82ed0-5d4b-4c01-949d-7fa26054536b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "10f92d6b-f7d1-47e9-ab49-d257ab1a739a",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "685ec6f2-ebab-485a-9e12-b38b27898499"
        },
        {
            "id": "d5af2a93-496b-41ea-b31a-caa7886739ff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "8a5a3fff-7bb5-4c5c-b542-2a19b0da81d9",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "685ec6f2-ebab-485a-9e12-b38b27898499"
        },
        {
            "id": "91734ed5-f8d6-49b6-a8b7-73fc99aa395e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "fb24ce73-cf76-453a-8551-08119588dd56",
            "enumb": 0,
            "eventtype": 4,
            "m_owner": "685ec6f2-ebab-485a-9e12-b38b27898499"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "756dc221-bc20-4fcd-95c4-65f99b5ee88f",
    "visible": true
}