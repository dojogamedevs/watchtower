{
    "id": "38965bc0-41bf-4656-b24e-9de61221a537",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_level_over",
    "eventList": [
        {
            "id": "6e43cee9-7c0c-424e-b98a-f894578469f1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "38965bc0-41bf-4656-b24e-9de61221a537"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "82a427cb-5368-4fa1-ae19-a111e3637a2c",
    "visible": true
}