{
    "id": "0e13e480-a36b-4ddc-800a-e11462f73084",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_pierce1",
    "eventList": [
        {
            "id": "69362f80-0975-4d49-afdb-a4744b2ca477",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0e13e480-a36b-4ddc-800a-e11462f73084"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "75ee5a79-72c4-4a2c-b567-32cea83bd138",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "57229ca2-e335-4ea7-a7a1-901df40f53fe",
    "visible": true
}