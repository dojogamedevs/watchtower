{
    "id": "0290f86a-096c-4793-93d6-384dbfe3227b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_pierce1d",
    "eventList": [
        {
            "id": "710fa804-c922-478b-8672-b6ba769451bc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "0290f86a-096c-4793-93d6-384dbfe3227b"
        },
        {
            "id": "4e4cf02c-e65f-44f2-93f7-dcb69279a501",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "0290f86a-096c-4793-93d6-384dbfe3227b"
        },
        {
            "id": "d183bd24-2ae2-44c7-b0f2-92ff0b701dbf",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "0290f86a-096c-4793-93d6-384dbfe3227b"
        },
        {
            "id": "ad6155a3-46ad-48b6-9725-2b2a302691db",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "0290f86a-096c-4793-93d6-384dbfe3227b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "57229ca2-e335-4ea7-a7a1-901df40f53fe",
    "visible": true
}