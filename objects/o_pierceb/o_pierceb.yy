{
    "id": "d07b384d-71d3-4f01-9e37-75e1a6668133",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_pierceb",
    "eventList": [
        {
            "id": "8c5700e7-ff5c-435b-9dd5-6248efed51d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "d07b384d-71d3-4f01-9e37-75e1a6668133"
        },
        {
            "id": "dea8cd79-6024-429b-a71e-1ba813004c5f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d07b384d-71d3-4f01-9e37-75e1a6668133"
        },
        {
            "id": "aac550ac-af4b-43ed-aad1-e7874cea2b3e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d07b384d-71d3-4f01-9e37-75e1a6668133"
        },
        {
            "id": "06d873f3-b7ef-457e-84de-ba81e14d7550",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "d07b384d-71d3-4f01-9e37-75e1a6668133"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f80786cf-f608-43c1-9cae-6ef18095a251",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d9434990-c811-4160-a28b-552f1fc96375",
    "visible": true
}