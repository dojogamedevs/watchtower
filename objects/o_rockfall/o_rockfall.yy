{
    "id": "3a16e31a-afb3-49d3-be5b-9d91fc5a3ece",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_rockfall",
    "eventList": [
        {
            "id": "156021ec-05d4-41f3-9044-7dc01afd6a76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3a16e31a-afb3-49d3-be5b-9d91fc5a3ece"
        },
        {
            "id": "c45a1172-cd3f-48d7-978c-f5e0ac1e14a9",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3a16e31a-afb3-49d3-be5b-9d91fc5a3ece"
        },
        {
            "id": "5323464f-c310-423c-a940-387f1b395bf3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "3a16e31a-afb3-49d3-be5b-9d91fc5a3ece"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "cd0ce9dd-9838-42f1-aef8-2c6c86160b7a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "a4992fe5-1221-47c8-9a9f-6c2c43e08194",
    "visible": true
}