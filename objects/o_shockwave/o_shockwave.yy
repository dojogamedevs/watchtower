{
    "id": "042e760f-c5aa-49d4-b6ce-b9123d03fcbf",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_shockwave",
    "eventList": [
        {
            "id": "a258d8f1-9b78-44ee-83bf-9e1bb8161fc7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "042e760f-c5aa-49d4-b6ce-b9123d03fcbf"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "75ee5a79-72c4-4a2c-b567-32cea83bd138",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "33c19ec2-797d-4940-ad17-b8d3fe9461e8",
    "visible": true
}