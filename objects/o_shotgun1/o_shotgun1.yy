{
    "id": "47afdd17-c85b-4709-af34-5e4f48a86c64",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_shotgun1",
    "eventList": [
        {
            "id": "9aca74c0-603a-4f66-8271-2bcd816c171d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "47afdd17-c85b-4709-af34-5e4f48a86c64"
        },
        {
            "id": "5875a104-7799-4abb-b58d-4df30b2139a8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "47afdd17-c85b-4709-af34-5e4f48a86c64"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "75ee5a79-72c4-4a2c-b567-32cea83bd138",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2c84bea8-f205-4f49-89c6-e6a2b2bcbecd",
    "visible": true
}