{
    "id": "d6de0abc-154d-4b4e-a0c2-2d5c67081829",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_shotgun1c",
    "eventList": [
        {
            "id": "a2968816-566b-484c-8dec-3496db5e5ee0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "d6de0abc-154d-4b4e-a0c2-2d5c67081829"
        },
        {
            "id": "98bba7a8-b2d2-419b-acf4-24d2c3a59133",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "d6de0abc-154d-4b4e-a0c2-2d5c67081829"
        },
        {
            "id": "a9e241c7-5ced-4adf-96f5-6bc5c8084dd5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "d6de0abc-154d-4b4e-a0c2-2d5c67081829"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2c84bea8-f205-4f49-89c6-e6a2b2bcbecd",
    "visible": true
}