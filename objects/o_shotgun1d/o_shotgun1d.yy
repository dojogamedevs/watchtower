{
    "id": "faedb245-b647-4202-8ca9-951dca993c2e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_shotgun1d",
    "eventList": [
        {
            "id": "92d4d89b-8e81-4255-ae50-21f1e518d2d7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "faedb245-b647-4202-8ca9-951dca993c2e"
        },
        {
            "id": "e09e0a84-8fc9-4874-984c-18f00a0d3559",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "faedb245-b647-4202-8ca9-951dca993c2e"
        },
        {
            "id": "21765baf-ac56-4331-957c-5329526a9dad",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "faedb245-b647-4202-8ca9-951dca993c2e"
        },
        {
            "id": "78e57044-2253-4c3b-9545-ae30087c6d2f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "faedb245-b647-4202-8ca9-951dca993c2e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "2c84bea8-f205-4f49-89c6-e6a2b2bcbecd",
    "visible": true
}