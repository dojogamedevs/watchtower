{
    "id": "1969cdd7-c1c8-48ad-a36b-b631d178e2bd",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_snipeb",
    "eventList": [
        {
            "id": "bb6f7e1b-5c64-4ab4-b2c1-cfa256699230",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 76,
            "eventtype": 8,
            "m_owner": "1969cdd7-c1c8-48ad-a36b-b631d178e2bd"
        },
        {
            "id": "46bf865f-6923-41d6-a030-aae9297b3b1a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1969cdd7-c1c8-48ad-a36b-b631d178e2bd"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f80786cf-f608-43c1-9cae-6ef18095a251",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1b2bc9e9-2db9-4596-b513-151d84b09433",
    "visible": true
}