{
    "id": "1903ab9f-2860-4bd7-9480-19ac5088273b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_sniper1",
    "eventList": [
        {
            "id": "a24b0911-3cc3-4cba-b721-1798d94f7e18",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1903ab9f-2860-4bd7-9480-19ac5088273b"
        },
        {
            "id": "97570ffd-17d2-4eb7-90e2-5c21d2655560",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "1903ab9f-2860-4bd7-9480-19ac5088273b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "75ee5a79-72c4-4a2c-b567-32cea83bd138",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fd6b0c75-aee1-4ebb-820d-bf50f3e269bf",
    "visible": true
}