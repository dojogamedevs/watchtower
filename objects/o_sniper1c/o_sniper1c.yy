{
    "id": "30c799d6-b988-4d1b-a813-270930c3a6de",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_sniper1c",
    "eventList": [
        {
            "id": "be390a6d-1129-4c5b-87f3-971cec249905",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "30c799d6-b988-4d1b-a813-270930c3a6de"
        },
        {
            "id": "2f1a39c2-ab53-4969-a504-db02b8a230f8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "30c799d6-b988-4d1b-a813-270930c3a6de"
        },
        {
            "id": "6b9fd0e1-0eba-4213-9e2c-71c23332076f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "30c799d6-b988-4d1b-a813-270930c3a6de"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fd6b0c75-aee1-4ebb-820d-bf50f3e269bf",
    "visible": true
}