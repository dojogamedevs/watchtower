{
    "id": "71ded136-04ad-432f-9b75-a27b61821deb",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_sniper1d",
    "eventList": [
        {
            "id": "df4a5cbd-de7c-4717-8021-722d3216d50f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "71ded136-04ad-432f-9b75-a27b61821deb"
        },
        {
            "id": "e34e7d35-62e2-48bd-b586-a994bfdf5f2e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "71ded136-04ad-432f-9b75-a27b61821deb"
        },
        {
            "id": "ab74b15c-0923-462b-9dd6-a44251abf223",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "71ded136-04ad-432f-9b75-a27b61821deb"
        },
        {
            "id": "2ca6703b-af88-4029-8947-9cbb5fb3ad39",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "71ded136-04ad-432f-9b75-a27b61821deb"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "fd6b0c75-aee1-4ebb-820d-bf50f3e269bf",
    "visible": true
}