if global.spawn = true {
	if (spawn_count < spawn_amount) and wave <= 10  {
		instance_create_layer(x,y,"Instances",o_enemy)
		spawn_count++;
	}
	if (spawn_count < spawn_amount) and wave > 10 and wave <= 24 {
		instance_create_layer(x,y,"Instances",o_enemyBIG)
		spawn_count++;
	}
	if (spawn_count < spawn_amount) and wave = 25 {
		instance_create_layer(x,y,"Instances",o_enemy_boss1)
		spawn_count++;
	}
}
alarm[0] = spawn_rate;
