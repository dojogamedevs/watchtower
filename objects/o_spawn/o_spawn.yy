{
    "id": "f867fbf3-afdb-47db-bbe4-9e3890b7f36c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_spawn",
    "eventList": [
        {
            "id": "df1da8ae-ae5b-4442-967a-9a8e4f12f6a3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "f867fbf3-afdb-47db-bbe4-9e3890b7f36c"
        },
        {
            "id": "1806cbec-c8fb-413e-a8e7-8086b27a696e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "f867fbf3-afdb-47db-bbe4-9e3890b7f36c"
        },
        {
            "id": "0de4ba96-0b44-4a0d-a891-1bea11067ec2",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 1,
            "eventtype": 2,
            "m_owner": "f867fbf3-afdb-47db-bbe4-9e3890b7f36c"
        },
        {
            "id": "cadc2679-0e8e-4ac0-bbb6-79982c7f8d69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "f867fbf3-afdb-47db-bbe4-9e3890b7f36c"
        },
        {
            "id": "6e0fafcb-d6e9-47f7-a142-b33c6c3017b4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 2,
            "m_owner": "f867fbf3-afdb-47db-bbe4-9e3890b7f36c"
        },
        {
            "id": "d2a6cd4b-7fb3-4d00-8aa0-c44b05d43047",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "f867fbf3-afdb-47db-bbe4-9e3890b7f36c"
        },
        {
            "id": "be5e7c4e-2c42-4f2a-ac1c-7d9123b53e9d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 16,
            "eventtype": 5,
            "m_owner": "f867fbf3-afdb-47db-bbe4-9e3890b7f36c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}