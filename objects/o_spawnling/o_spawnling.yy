{
    "id": "fdd996aa-f3fb-4a1c-8d0b-ac5d2ee570f5",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_spawnling",
    "eventList": [
        {
            "id": "652a8878-7c07-4189-8520-853245680222",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "fdd996aa-f3fb-4a1c-8d0b-ac5d2ee570f5"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "bbebf1e1-d28c-4719-84bf-7c649349b2af",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "30bb9cf4-68da-413e-8810-f5b153228665",
    "visible": true
}