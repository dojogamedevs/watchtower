{
    "id": "f6fcd3bd-75bd-46a4-ad4e-3fa3f28fca99",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_start",
    "eventList": [
        {
            "id": "8ffb4857-d0bf-45cf-b723-5909fb7623ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 6,
            "m_owner": "f6fcd3bd-75bd-46a4-ad4e-3fa3f28fca99"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b9e6c861-9b1c-4d33-9e47-d3f9fe8c37be",
    "visible": true
}