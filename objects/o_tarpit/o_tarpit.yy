{
    "id": "10f92d6b-f7d1-47e9-ab49-d257ab1a739a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_tarpit",
    "eventList": [
        {
            "id": "54f1037e-f1f8-4f48-8784-577fc3974f7f",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "10f92d6b-f7d1-47e9-ab49-d257ab1a739a"
        },
        {
            "id": "3667502a-aaec-46f6-9a9b-93409f15c50a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "10f92d6b-f7d1-47e9-ab49-d257ab1a739a"
        },
        {
            "id": "0844ff12-6c28-4f8b-bfff-34169bef8a2b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "10f92d6b-f7d1-47e9-ab49-d257ab1a739a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "cd0ce9dd-9838-42f1-aef8-2c6c86160b7a",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "4f1fba2d-6f91-4fb1-a0c5-e1656f5f5767",
    "visible": true
}