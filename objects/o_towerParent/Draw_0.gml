///@description Tower info drawing
draw_self();
if selected {
	draw_circle(x,y,range,true);
	if (minrange > 0) {
		draw_circle(x,y,minrange,true);
	}
	if instance_exists(objecttoshoot) {
		draw_line(x, y, objecttoshoot.x, objecttoshoot.y);
	}

	draw_set_color(c_aqua);
	draw_rectangle(x-16, y-16, x+16, y+16, true);
	draw_set_color(c_white);
}

if path {
	draw_path(path, x, y, true);
}