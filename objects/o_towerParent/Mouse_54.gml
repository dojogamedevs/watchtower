/// @description Insert description here
// You can write your code in this editor
if mobile and selected {
	var npos = snap_to_grid(mouse_x, mouse_y);
	// remove self from grid
	mp_grid_clear_cell(global.grid, x div 32, y div 32);
	path = noone;
	path = path_add();
	show_debug_message("pos: " + string(x) + ", " + string(y));
	show_debug_message("new pos: " + string(npos[0]) + ", " + string(npos[1]));
	if mp_grid_path(global.grid, path, x, y, npos[0], npos[1], false) {
		path_start(path, tspeed, path_action_stop, false);
		next_state = "moving";
		mp_grid_add_cell(global.grid, npos[0] div 32, npos[1] div 32);
	}
}