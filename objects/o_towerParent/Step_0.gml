/// @description Targeting AI
if state != next_state {
	state = next_state;
}


if (minrange == 0) {
	var en = instance_nearest(x,y,o_enemy);
	if(en != noone and point_distance(x,y,en.x,en.y) <= range) {
		
		if(!shooting){
			alarm[0] = 1;
			shooting = true;
		}
		
		objecttoshoot = en;
		
	}else{
		shooting = false;
		objecttoshoot = noone;
	}
} else {
	var badguylist = ds_priority_create();
	with (o_enemy) {
		var pd = point_distance(x, y, other.x, other.y);
		if (pd <= other.range and pd >= other.minrange) {
			ds_priority_add(badguylist, id, pd);
		}
	}
	if (ds_priority_size(badguylist) > 0) {
		var en = ds_priority_delete_min(badguylist);
		objecttoshoot = en;
		if (!shooting) {
			shooting = true;
			alarm[0] = 1;
		}
	} else {
		shooting = false;
		objecttoshoot = noone;
	}
}