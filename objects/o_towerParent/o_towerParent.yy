{
    "id": "75ee5a79-72c4-4a2c-b567-32cea83bd138",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_towerParent",
    "eventList": [
        {
            "id": "22f865b9-8ba2-4a55-89b6-ca1c69fbf11b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "75ee5a79-72c4-4a2c-b567-32cea83bd138"
        },
        {
            "id": "269acb06-81fb-4a23-8288-3d0469240600",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "75ee5a79-72c4-4a2c-b567-32cea83bd138"
        },
        {
            "id": "36219d93-8377-4d26-bde2-0c8820a6330c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "75ee5a79-72c4-4a2c-b567-32cea83bd138"
        },
        {
            "id": "1cdc0548-76c5-4960-8c0b-3294de67d9b7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 54,
            "eventtype": 6,
            "m_owner": "75ee5a79-72c4-4a2c-b567-32cea83bd138"
        },
        {
            "id": "5858b099-c1b2-404b-b864-9d4c8cf15dae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 8,
            "eventtype": 7,
            "m_owner": "75ee5a79-72c4-4a2c-b567-32cea83bd138"
        },
        {
            "id": "7dbe5f62-8737-4ed3-94a4-4787fa043fd8",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "75ee5a79-72c4-4a2c-b567-32cea83bd138"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}