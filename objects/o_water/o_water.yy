{
    "id": "3ecae0c7-1bbf-4459-81d3-e9441c45ce37",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_water",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "96a0eca0-16fc-4f96-a659-c07265d1ae29",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d71b8d1f-51b6-4271-8917-f5b34bb0f3fd",
    "visible": true
}