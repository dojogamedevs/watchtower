{
    "id": "62ce091f-4c5b-4113-bd69-c36a19cb059a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_wavebutton",
    "eventList": [
        {
            "id": "a5ccc474-8535-4a40-8de6-d8ba7c1b2144",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "62ce091f-4c5b-4113-bd69-c36a19cb059a"
        },
        {
            "id": "3056f703-5c98-4450-b106-da63754be5f3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "62ce091f-4c5b-4113-bd69-c36a19cb059a"
        },
        {
            "id": "2bf4e39f-5ea0-4701-922a-e986b7266665",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "62ce091f-4c5b-4113-bd69-c36a19cb059a"
        },
        {
            "id": "7495c78c-0cbc-4794-a430-52d071344683",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "62ce091f-4c5b-4113-bd69-c36a19cb059a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "b8aec022-30fe-4993-bc47-9d2ecbab39a8",
    "visible": true
}