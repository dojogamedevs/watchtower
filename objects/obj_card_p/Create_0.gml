enum cardstate {
	deck,
	drawn,
	hand,
	dragged,
	placing,
	discard,
	placed
}

enum cardclass {
	standard,
	dwarf
}

enum cardrarity {
	common,
	uncommon,
	rare
}

depth = -y;

start_x = x;
start_y = y;
played = false
flip=0;
state = cardstate.deck;
next_state = state;
offset_x = 0;
offset_y = 0;
card_name = "default";
card_type = "default";
cardinality = 0;
child_tower = noone;
cost = 1;
current_player = instance_find(obj_player, 0);
done = false;
class = cardclass.standard;
rarity = cardrarity.common;