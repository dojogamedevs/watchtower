/// @description Insert description here
// You can write your code in this editor
var dest_x = 50 + 110 * cardinality;
var dest_y = 600;
if state != next_state {
	show_debug_message("old state: " + string(state) + ", new state: " + string(next_state));
	state = next_state;
}

if (state == cardstate.drawn) {
	if (distance_to_point(dest_x, dest_y) >= 2) {
		move_towards_point(dest_x, dest_y, distance_to_point(dest_x, dest_y) / 100 + 3)
	} else {
		next_state = cardstate.hand;
	}
}
if (state == cardstate.deck) {
	x = -100;
	y = -100;
}
if state == cardstate.hand {
	speed = 0
	x = dest_x
	y = dest_y
} 
if state == cardstate.dragged {
	image_xscale = 1.2;
	image_yscale = 1.2;
	if (mouse_y > 500) {
		//x = mouse_x - offset_x;
		//y = mouse_y - offset_y;
	} else {
		next_state = cardstate.placing;
	}
}
if not (state == cardstate.dragged || state == cardstate.placing) {
	image_xscale = 1.0;
	image_yscale = 1.0;
}

if state == cardstate.placing {
	if card_type == "turret" {
		if child_tower == noone {
			child_tower = instance_create_layer(mouse_x, mouse_y, "Instances", obj_tower_drag);
			child_tower.type = card_name;
			child_tower.sprite_index = s_ballista;
			child_tower.parent_card = self;
		}
	}
	if card_type == "trap" {
		if child_tower == noone {
			child_tower = instance_create_layer(mouse_x, mouse_y, "Instances", obj_trap_drag);
			child_tower.type = card_name;
			child_tower.sprite_index = s_rockfall;
			child_tower.parent_card = self;
		}
}
}

if state == cardstate.discard {
	x = 700;
	y = 550;
}

if state == cardstate.placed {
	next_state = cardstate.discard;
	current_player.mana -= cost;
	ds_list_delete(current_player.hand, ds_list_find_index(current_player.hand, self));
	current_player.hand_dirty = true;
	ds_list_add(current_player.discard, self);
}
