{
    "id": "b9d693a8-d11c-4cc4-86cf-2ccad033873c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_card_p",
    "eventList": [
        {
            "id": "f396f9a9-fecf-4ce2-acb2-35d86f6fac48",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "b9d693a8-d11c-4cc4-86cf-2ccad033873c"
        },
        {
            "id": "c00c8abe-a94d-415a-8918-c241057fdf29",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "b9d693a8-d11c-4cc4-86cf-2ccad033873c"
        },
        {
            "id": "10ee2bee-d910-42b5-91d2-ba09c3478fb0",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "b9d693a8-d11c-4cc4-86cf-2ccad033873c"
        },
        {
            "id": "af4d30e3-d6ce-409f-bb73-ff21762a6610",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "b9d693a8-d11c-4cc4-86cf-2ccad033873c"
        },
        {
            "id": "c1dd5f3d-b249-4185-b408-d7b8bf800761",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 5,
            "eventtype": 6,
            "m_owner": "b9d693a8-d11c-4cc4-86cf-2ccad033873c"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}