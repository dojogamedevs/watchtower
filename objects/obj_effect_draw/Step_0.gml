/// @description Insert description here
// You can write your code in this editor

event_inherited();

if state == effectstate.active {
	draw(amount);
	var current_player = get_player();
	current_player.hand_dirty = true;
	next_state = effectstate.done;
}