{
    "id": "7c73e87c-6476-44c5-ac7b-485bbd73050f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_effect_draw",
    "eventList": [
        {
            "id": "3d72e2eb-d290-45aa-98af-dcfb68b976ab",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "7c73e87c-6476-44c5-ac7b-485bbd73050f"
        },
        {
            "id": "ded7471d-1722-4857-b615-b0ad1aa6fac4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "7c73e87c-6476-44c5-ac7b-485bbd73050f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4661efc6-e2c4-48a7-9207-bfa52a78eeae",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}