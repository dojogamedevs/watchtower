{
    "id": "4cbe4217-cb70-4b67-a6b1-1d9c30e56f1a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_effect_fallingrocks",
    "eventList": [
        {
            "id": "ef9beaed-2844-445f-902e-33f6702a3026",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "4cbe4217-cb70-4b67-a6b1-1d9c30e56f1a"
        },
        {
            "id": "aa039e60-efdd-4305-bbf8-42f96f9a20fa",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "4cbe4217-cb70-4b67-a6b1-1d9c30e56f1a"
        },
        {
            "id": "381b8d51-2115-4c8e-8577-076e4c1b7fff",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "4cbe4217-cb70-4b67-a6b1-1d9c30e56f1a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "36049ee5-b2ca-448e-8216-cb28ce0fa643",
    "visible": true
}