/// @description Insert description here
// You can write your code in this editor

event_inherited();

if state == "active" {
	var hite = collision_circle(x, y, 10, o_enemy, false, true);
	if (hite != noone) {
		hite.hp -= damage;
	}
	next_state = "linger";
}

if state == "linger" {
	age += 1;
	if age > lifetime {
		next_state = "done";
	}
}