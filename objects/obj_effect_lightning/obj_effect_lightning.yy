{
    "id": "ad1cfecf-5e67-4c13-ac9b-ddbcd88d2454",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_effect_lightning",
    "eventList": [
        {
            "id": "2e2a5c01-d031-46bc-904a-f794c93ea571",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "ad1cfecf-5e67-4c13-ac9b-ddbcd88d2454"
        },
        {
            "id": "6f35d4c6-46b0-4cff-af38-502119cd1769",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "ad1cfecf-5e67-4c13-ac9b-ddbcd88d2454"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4661efc6-e2c4-48a7-9207-bfa52a78eeae",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "55388686-9a3e-42ca-8a01-849a8f2c0dff",
    "visible": true
}