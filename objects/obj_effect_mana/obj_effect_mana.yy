{
    "id": "c588f7e8-4ee0-4ac2-b88f-674a5c881d0d",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_effect_mana",
    "eventList": [
        {
            "id": "3de80420-651b-48be-a04e-2a7137dc3293",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c588f7e8-4ee0-4ac2-b88f-674a5c881d0d"
        },
        {
            "id": "c5e793f7-c58e-4117-a172-d10776beec76",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c588f7e8-4ee0-4ac2-b88f-674a5c881d0d"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "4661efc6-e2c4-48a7-9207-bfa52a78eeae",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": false
}