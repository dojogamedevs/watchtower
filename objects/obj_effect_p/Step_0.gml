if state != next_state {
	state = next_state;
}

if state == "done" {
	show_debug_message("remove effect: " + name);
	instance_destroy();
}