/// @description Insert description here
// You can write your code in this editor
current_deck = ds_list_create();
cards_owned = ds_list_create();
hand = ds_list_create();
discard = ds_list_create();
mana = 2;
base_mana = 2;
hand_dirty = false;
health_stage = 1;
next_turn_effects = ds_queue_create();
global.player = self;
has_greedy_pot = true;
//generate_level()
state = "idle";
next_state = "idle";
selected_tower = noone;
global.grid = mp_grid_create(0, 0, 20, 15, 32, 32);
mp_grid_add_instances(global.grid, o_region, false);
global.mans = 25;
global.mana = 5;
global.coins = 100;