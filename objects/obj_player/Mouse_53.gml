/// @description Insert description here
// You can write your code in this editor
if selected_tower != noone {
	selected_tower.selected = false;
}
if mouse_y < 500 and state == "idle" or state == "selected" {
	var tower = instance_position(mouse_x, mouse_y, o_towerParent);
	if tower != noone {
		show_debug_message("selected somebody!");
		next_state = "selected";
		selected_tower = tower;
		selected_tower.selected = true;
	} else {
		next_state = "idle";
		
		selected_tower = noone;
	}
}

