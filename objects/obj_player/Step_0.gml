/// @description Insert description here
// You can write your code in this editor
if (global.mans <= 0) {
	show_message("you lose dummy!");
	room_goto_previous();
}
if (global.mans >= 16) && (global.mans <= 20) {
	base_mana = 3;
}
if (global.mans >= 11) && (global.mans <= 15) {
	base_mana = 4;
}
if (global.mans >= 6) && (global.mans <= 10) {
	base_mana = 5;
}
if (global.mans <= 5) {
	base_mana = 6;
}

if state != next_state {
	show_debug_message("player old state: " + state + ", new state: " + next_state);
	state = next_state;
}

if hand_dirty {
	// rebalance hand
	for (i = 0; i < ds_list_size(hand); i++) {
		var ccard = ds_list_find_value(hand, i);
		ccard.cardinality = i;
	}
	hand_dirty = false;
}

if has_greedy_pot {
	if state != "turn_start" and ds_list_size(hand) == 0 {
		draw(2);
	}
}
if state == "turn_start" && global.mans >= 11 {
	draw(5);
	mana = base_mana;
	hand_dirty = true;
	next_state = "idle";
	while (not ds_queue_empty(next_turn_effects)) {
		var effect = ds_queue_dequeue(next_turn_effects);
		effect.next_state = effectstate.active;
	}
}
if state == "turn_start" && global.mans <= 10 && global.mans >= 6 {
	draw(6);
	mana = base_mana;
	hand_dirty = true;
	next_state = "idle";
	while (not ds_queue_empty(next_turn_effects)) {
		var effect = ds_queue_dequeue(next_turn_effects);
		effect.next_state = "active";
	}
}
if state == "turn_start" && global.mans <= 5 {
	draw(7);
	mana = base_mana;
	hand_dirty = true;
	next_state = "idle";
	while (not ds_queue_empty(next_turn_effects)) {
		var effect = ds_queue_dequeue(next_turn_effects);
		effect.next_state = "active";
	}
}

if state == "turn_over" {
	for (i = 0; i < ds_list_size(hand); i++) {
		var hcard = ds_list_find_value(hand, i);
		hcard.next_state = cardstate.discard;
		ds_list_add(discard, hcard);
	}
	hand = ds_list_create();
	next_state = "turn_start";
}
