{
    "id": "3ded01d4-88db-4bda-b824-565b403f9a1e",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_player",
    "eventList": [
        {
            "id": "1e5d7259-1f0c-48fa-8097-7f484ce29d26",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "3ded01d4-88db-4bda-b824-565b403f9a1e"
        },
        {
            "id": "cc443351-2a91-47c6-bb37-2e35d223899e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 64,
            "eventtype": 8,
            "m_owner": "3ded01d4-88db-4bda-b824-565b403f9a1e"
        },
        {
            "id": "1d35f060-7f05-4032-ac1d-c848c1fe567d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "3ded01d4-88db-4bda-b824-565b403f9a1e"
        },
        {
            "id": "d436db0f-5b0a-488e-b1a7-564d2f684141",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 53,
            "eventtype": 6,
            "m_owner": "3ded01d4-88db-4bda-b824-565b403f9a1e"
        },
        {
            "id": "1a15767c-707a-4174-b3a0-5b2b89d1925a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 32,
            "eventtype": 5,
            "m_owner": "3ded01d4-88db-4bda-b824-565b403f9a1e"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": true,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e1fb48f6-5bac-4c9f-86b4-277c40e7ca8d",
    "visible": true
}