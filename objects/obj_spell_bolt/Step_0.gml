/// @description Insert description here
// You can write your code in this editor
event_inherited();

if state == cardstate.placed {
	var hite = collision_circle(mouse_x, mouse_y, 10, o_enemy, false, true);
	if (hite == noone) {
		show_debug_message("no hit!");
		next_state = cardstate.hand;
		state = cardstate.hand;
	} else {
		var lt = instance_create_depth(0, 0, 0, obj_effect_lightning);
		lt.next_state = "active";
		lt.x = mouse_x;
		lt.y = mouse_y;
	}
}