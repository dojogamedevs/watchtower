{
    "id": "95b3c656-778c-498a-a018-ef6468ee0316",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_spell_bolt",
    "eventList": [
        {
            "id": "f119f30c-7420-4938-b331-900f66befb10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "95b3c656-778c-498a-a018-ef6468ee0316"
        },
        {
            "id": "bf884cf6-f045-408c-88b9-a15471fac449",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "95b3c656-778c-498a-a018-ef6468ee0316"
        },
        {
            "id": "69bcc199-6cd6-4996-a832-ddd26dff428d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "95b3c656-778c-498a-a018-ef6468ee0316"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f94f0b0d-fbe3-4632-83e5-880ca6a2aa0d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e296e041-17b6-4038-b6dc-08903bd62f99",
    "visible": true
}