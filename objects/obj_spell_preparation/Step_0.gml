/// @description Insert description here
// You can write your code in this editor

if state == cardstate.placed {
	var draw_effect = instance_create_depth(0, 0, 0, obj_effect_draw);
	draw_effect.amount = 1;
	ds_queue_enqueue(current_player.next_turn_effects, draw_effect);
	var mana_effect = instance_create_depth(0, 0, 0, obj_effect_mana);
	mana_effect.amount = 2;
	ds_queue_enqueue(current_player.next_turn_effects, mana_effect);
}

event_inherited();