{
    "id": "1db3ad7c-07bc-4709-bef6-0c955597a2a9",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_spell_preparation",
    "eventList": [
        {
            "id": "98fdf827-e3d8-4b90-b4af-37d43a64b0ee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "1db3ad7c-07bc-4709-bef6-0c955597a2a9"
        },
        {
            "id": "8b37ecf1-f4af-4843-a8ee-2f9498050fc7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "1db3ad7c-07bc-4709-bef6-0c955597a2a9"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "f94f0b0d-fbe3-4632-83e5-880ca6a2aa0d",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "428da71b-5816-45e6-9b6d-f6d35cd9cc9d",
    "visible": true
}