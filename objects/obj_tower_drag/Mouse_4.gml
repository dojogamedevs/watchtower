/// @description Insert description here
// You can write your code in this editor
if (color = c_white) {
	instance_destroy();
	var otype = noone;
	switch type {
		case "ballista":
			otype = o_ballista1;
			break;
		case "shotgun":
			otype = o_shotgun1;
			break;
		case "sniper":
			otype = o_sniper1;
			break;
		case "Javelin":
			otype = o_pierce1;
			break;
		case "Catapult":
			otype = o_catapult1;
			break;
	}
	instance_create_depth(mouse_x,mouse_y,-1,otype);
}