/// @description Insert description here
// You can write your code in this editor
if (color == c_white) and y < 500 {
	var otype = noone;
	switch type {
		case "ballista":
			otype = o_ballista1;
			break;
		case "scrapgun":
			otype = o_shotgun1;
			break;
		case "sniper":
			otype = o_sniper1;
			break;
		case "Javelin":
			otype = o_pierce1;
			break;
		case "Outpost":
			otype = o_outpost;
			break;
		case "Catapult":
			otype = o_catapult1
			break;
		case "Shockwave":
			otype = o_shockwave
			break;
	}
	instance_create_depth(x,y,-1,otype);
	var player = get_player();
	parent_card.done = true;
	parent_card.next_state = cardstate.placed;
	instance_destroy(self);
	parent_card.child_tower = noone;
} else {
	parent_card.next_state = cardstate.hand;
	instance_destroy(self);
	parent_card.child_tower = noone;
}