{
    "id": "980f46e2-d2ba-4507-8eae-724648742b8f",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_tower_drag",
    "eventList": [
        {
            "id": "9fa41083-ee5f-4448-8884-3e714981f461",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "980f46e2-d2ba-4507-8eae-724648742b8f"
        },
        {
            "id": "f0a5ce40-4d75-436a-bdcd-d4e4e5921fa5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "980f46e2-d2ba-4507-8eae-724648742b8f"
        },
        {
            "id": "67394cee-e964-4076-b0f0-1986dc40c8e3",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "980f46e2-d2ba-4507-8eae-724648742b8f"
        },
        {
            "id": "7f529bde-69ca-4824-82d9-488ecfc01a93",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 56,
            "eventtype": 6,
            "m_owner": "980f46e2-d2ba-4507-8eae-724648742b8f"
        },
        {
            "id": "240219e4-4cd1-4522-b4de-c5b8a9ae6316",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "980f46e2-d2ba-4507-8eae-724648742b8f"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}