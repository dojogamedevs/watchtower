/// @description Insert description here
// You can write your code in this editor
if (color = c_white) {
	instance_destroy();
	var otype = noone;
	switch type {
		case "Rockfall Trap":
			otype = o_rockfall;
			break;
		case "Tar Pit":
			otype= o_tarpit;
			break;
	}
	instance_create_depth(mouse_x,mouse_y,-1,otype);
}