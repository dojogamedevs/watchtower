/// @description Insert description here
// You can write your code in this editor
if (color == c_white) and y < 500 {
	var otype = noone;
	switch type {
		case "Rockfall Trap":
			otype = o_rockfall;
			break;
		case "Tar Pit":
			otype= o_tarpit;
			break;
	}
	instance_create_layer(x,y,"Traps",otype);
	var player = get_player();
	parent_card.done = true;
	instance_destroy(self);
	parent_card.child_tower = noone;
	parent_card.next_state = cardstate.placed;
} else {
	parent_card.next_state = cardstate.hand;
	instance_destroy(self);
	parent_card.child_tower = noone;
}