{
    "id": "c1e93f31-94d8-489d-86f8-396ef1b94bd0",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "obj_trap_drag",
    "eventList": [
        {
            "id": "2ce42b64-cbfc-475f-a8ea-9192175152ae",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "c1e93f31-94d8-489d-86f8-396ef1b94bd0"
        },
        {
            "id": "58056248-56ab-4923-a9be-ed791400d598",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "c1e93f31-94d8-489d-86f8-396ef1b94bd0"
        },
        {
            "id": "ce754b1f-cabb-4efb-9b6c-08664da2c954",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "c1e93f31-94d8-489d-86f8-396ef1b94bd0"
        },
        {
            "id": "702f6992-1535-45e9-8ce4-a7d81217ae0a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "c1e93f31-94d8-489d-86f8-396ef1b94bd0"
        },
        {
            "id": "bc3e6a73-064b-4947-9569-e8b278d2f6b1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 4,
            "eventtype": 6,
            "m_owner": "c1e93f31-94d8-489d-86f8-396ef1b94bd0"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}