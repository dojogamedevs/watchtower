{
    "id": "5e95ea0c-47ba-413a-b1e8-08716942eca8",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "path1",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "52334eca-1f39-428b-96f9-bf12c6655961",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 736,
            "y": 96,
            "speed": 100
        },
        {
            "id": "27f542f7-d4a5-4d5e-b019-60da739e98e4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 718.166565,
            "y": 110.996307,
            "speed": 100
        },
        {
            "id": "6dcc4fba-1ce3-46b1-b5d4-846c074a26c4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 240.084351,
            "y": 110.996307,
            "speed": 100
        },
        {
            "id": "858765fe-3ee1-4f4d-9a32-7238073c686a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 238.714539,
            "y": 205.516846,
            "speed": 100
        },
        {
            "id": "b68fb83b-b874-4104-8e11-62d0714bfdb3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 460.632324,
            "y": 206.886719,
            "speed": 100
        },
        {
            "id": "a499141e-4ec4-4d13-b26d-2595f0e5dc07",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 462.0022,
            "y": 335.653839,
            "speed": 100
        },
        {
            "id": "2c6345bc-b0c1-4836-a8a1-ec0b6c4bd702",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 400.988525,
            "y": 338.3936,
            "speed": 100
        },
        {
            "id": "ad16cf62-9630-4c8b-b7e6-26c3753fdaca",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 400.358337,
            "y": 400.0374,
            "speed": 100
        },
        {
            "id": "d8512210-88ba-4b71-ac5c-2f6d3996d46f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 592.1392,
            "y": 397.2977,
            "speed": 100
        },
        {
            "id": "fbd442f4-df8b-41ff-891b-744b8282b372",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 589.3995,
            "y": 334.284,
            "speed": 100
        },
        {
            "id": "e8201c88-7414-4d6e-914e-c2f3c3b9b74e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 527.0159,
            "y": 331.54425,
            "speed": 100
        },
        {
            "id": "5b306896-8fe9-4faf-bf58-92709e74c6c7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 526.385742,
            "y": 208.256592,
            "speed": 100
        },
        {
            "id": "6a1a8920-ea28-4b45-940d-b31f437fac73",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 655.1529,
            "y": 208.256592,
            "speed": 100
        },
        {
            "id": "c35431bf-3b43-429c-9ed4-76fb46b9758e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 652.413147,
            "y": 463.051147,
            "speed": 100
        },
        {
            "id": "b64f13b6-f2d5-4576-9667-8c12a6de7f8c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 337.454224,
            "y": 465.270325,
            "speed": 100
        },
        {
            "id": "eebf19d5-c5f4-41f2-bbec-11906b57eb51",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 334.604919,
            "y": 627.4347,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}