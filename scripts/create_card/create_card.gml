var card_name = argument0;
var card_type = argument1;

show_debug_message("create card " + card_name);

var new_card = instance_create_layer(50, room_height - 100, "Instances", obj_card_p);
new_card.card_name = card_name;
new_card.card_type = card_type;

// assign sprites
switch card_name {
	case "ballista":
		new_card = instance_create_layer(50, room_height - 100, "Instances", o_c_ballista);
		return new_card;
		break;
	case "sniper":
		new_card = instance_create_layer(50, room_height - 100, "Instances", o_c_sniper);
		return new_card;
		break;
	case "scrapgun":
		new_card = instance_create_layer(50, room_height - 100, "Instances", o_c_shotgun);
		return new_card;
		break;
	case "Preparation":
		new_card = instance_create_layer(50, room_height - 100, "Instances", obj_spell_preparation);
		return new_card;
		break;
	case "Javelin":
		new_card = instance_create_layer(50, room_height - 100, "Instances",o_c_pierce);
		return new_card;
		break;
	case "Rockfall Trap":
		new_card = instance_create_layer(50, room_height - 100, "Instances",o_c_rockfall);
		return new_card;
		break;
	case "Outpost":
		new_card = instance_create_layer(50, room_height - 100, "Instances", o_c_outpost);
		return new_card;
		break;
	case "BOLT":
		new_card = instance_create_layer(50, room_height - 100, "Instances", obj_spell_bolt);
		return new_card;
		break;
	case "Tar Pit":
		new_card = instance_create_layer(50, room_height - 100, "Instances", o_c_tar);
		return new_card;
		break;
	case "Catapult":
		new_card = instance_create_layer(50, room_height - 100, "Instances", o_c_catapult);
		return new_card;
		break;
	case "Shockwave":
		new_card = instance_create_layer(50, room_height - 100, "Instances", o_c_shockwave);
		return new_card;
		break;
}

show_error("Card doesn't exist! : " + card_name, true);