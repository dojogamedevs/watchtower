// card is [name, type, class, rarity]
global.cardlist = [
["ballista", "turret", "basic", "common"],
["sniper", "turret", "basic", "common"],
["scrapgun", "turret", "basic", "common"],
["Preparation", "spell", "basic", "common"],
["Javelin", "turret", "basic", "common"],
["Rockfall Trap", "trap", "basic", "common"],
["Outpost", "turret", "basic", "common"],
["BOLT", "spell", "basic", "common"],
["Tar Pit", "trap", "basic", "common"],
["Catapult", "turret", "basic", "common"],
["Shockwave", "turret", "basic", "common"]
];