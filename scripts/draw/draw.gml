var num=argument0;

var player = get_player();

if ds_list_size(player.current_deck) == 0 {
	// shuffle discard into deck
	for (i = 0; i < ds_list_size(player.discard); i++) {
		var dcard = ds_list_find_value(player.discard, i);
		dcard.next_state = cardstate.deck;
		dcard.state = cardstate.deck;
		dcard.x = 50;
		dcard.y = room_height - 100;
	}
	ds_list_shuffle(player.discard);
	player.current_deck = player.discard;
	player.discard = ds_list_create();
}

// find a card in the deck
for (j = 0; j < ds_list_size(player.current_deck); j++) {
	var card_draw = ds_list_find_value(player.current_deck, j);
		
	if card_draw.state == cardstate.deck {
		card_draw.next_state = cardstate.drawn;
		ds_list_delete(player.current_deck, j);
		card_draw.x = 50;
		card_draw.y = room_height - 100;
		ds_list_add(player.hand, card_draw);
		break;
	}
}

var decksz = 0;
for (i = 0; i < ds_list_size(player.current_deck); i++) {
	var card = ds_list_find_value(player.current_deck, i);
	if card.state == cardstate.deck {
		decksz += 1;
	}
}
show_debug_message("new decksz: " + string(decksz));
var deckg = instance_find(o_deckgen, 0);
deckg.deck_size = decksz;

player.hand_dirty = true;

if num > 1 {
	draw(num - 1);
}