var tile_layer = layer_get_id("Tiles_1");
var tm = layer_tilemap_get_id(tile_layer);
var ts = tilemap_get_tileset(tm);
T_ROOM_WIDTH = 32;
T_ROOM_HEIGHT = 18;

for (i = 0; i < T_ROOM_WIDTH; i++) {
	for (j = 0; j < T_ROOM_HEIGHT; j++) {
		var td = tilemap_get(tm, 0, 0);
		td = tile_set_index(td, 1);
		tilemap_set(tm, td, i, j);
	}
}