if instance_exists(objecttoshoot) {
	shoot_bullet_position(objecttoshoot.x, objecttoshoot.y, 0);
} else {
	shooting = false;
}

return shooting;