// called by a tower

var ox = argument0;
var oy = argument1;
var dx = argument2;

var bullet = instance_create_depth(x,y,-9,bobj);
bullet.speed = bspeed;
bullet.direction = point_direction(x, y, ox, oy) + dx;