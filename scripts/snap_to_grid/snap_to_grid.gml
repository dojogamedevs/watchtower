dx = argument0 - 16
dy = argument1 - 16

GRID_WIDTH = 32
GRID_HEIGHT = 32

rx = round(dx / GRID_WIDTH) * GRID_WIDTH + 16
ry = round(dy / GRID_HEIGHT) * GRID_HEIGHT + 16

return [rx, ry]