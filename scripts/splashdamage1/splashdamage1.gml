epicenter_X = argument0;
epicenter_Y = argument1;

zone_01 = 20;
zone_02 = 40;
zone_03 = 60;

totalenemies = instance_number(o_enemy) - 1;
for (i = totalenemies; i >= 0; i--)
{
  target = instance_find(o_enemy, i);
  distance = point_distance(epicenter_X, epicenter_Y, target.x, target.y)

  if ( distance < zone_01 )
  {
    target.hp -= argument2;
  }
  else if ( distance >= zone_01 && distance < zone_02)
  {
    target.hp -= argument3;
  }
  if ( distance >= zone_02 && distance < zone_03)
  {
    target.hp -= argument4;
  }
}