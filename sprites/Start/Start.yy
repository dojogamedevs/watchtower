{
    "id": "b9e6c861-9b1c-4d33-9e47-d3f9fe8c37be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "Start",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 99,
    "bbox_left": 0,
    "bbox_right": 249,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e28a3c8b-02fb-42b2-8600-827e3960046b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b9e6c861-9b1c-4d33-9e47-d3f9fe8c37be",
            "compositeImage": {
                "id": "e4883b8e-21f9-4cb1-903f-365135bcbdcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e28a3c8b-02fb-42b2-8600-827e3960046b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "85a913f8-5c89-4e79-96cf-3654d36cadfd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e28a3c8b-02fb-42b2-8600-827e3960046b",
                    "LayerId": "bb0dd225-9f47-4f3f-81af-12e38f485344"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 100,
    "layers": [
        {
            "id": "bb0dd225-9f47-4f3f-81af-12e38f485344",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b9e6c861-9b1c-4d33-9e47-d3f9fe8c37be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 250,
    "xorig": 0,
    "yorig": 0
}