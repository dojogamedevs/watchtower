{
    "id": "d53ae39b-0f4d-44ca-a5c7-f45afa1ee290",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "deck",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 69,
    "bbox_left": 0,
    "bbox_right": 66,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11be101c-7647-4171-8bb1-739e832c5d34",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d53ae39b-0f4d-44ca-a5c7-f45afa1ee290",
            "compositeImage": {
                "id": "27b22c5a-f4fc-4800-ab04-c79853cc61b4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11be101c-7647-4171-8bb1-739e832c5d34",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "97e89035-e6a1-434c-b2ca-3dcb9671ad5e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11be101c-7647-4171-8bb1-739e832c5d34",
                    "LayerId": "2c0bf1d3-a375-4134-aa13-1530b7f5c4cf"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 70,
    "layers": [
        {
            "id": "2c0bf1d3-a375-4134-aa13-1530b7f5c4cf",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d53ae39b-0f4d-44ca-a5c7-f45afa1ee290",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 70,
    "xorig": 0,
    "yorig": 0
}