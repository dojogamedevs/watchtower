{
    "id": "539e3bcf-2764-4c61-84b6-6b377b9b6bca",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_ballista",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a74abb1f-2013-4666-85f5-75a9ed39d462",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "539e3bcf-2764-4c61-84b6-6b377b9b6bca",
            "compositeImage": {
                "id": "ca50fd74-7805-47c1-b8a7-c923323250cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a74abb1f-2013-4666-85f5-75a9ed39d462",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2de37d77-e05e-4cd3-bbfa-f7d2810abd0a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a74abb1f-2013-4666-85f5-75a9ed39d462",
                    "LayerId": "6ba7a099-df81-46be-a026-0adcf4810710"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "6ba7a099-df81-46be-a026-0adcf4810710",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "539e3bcf-2764-4c61-84b6-6b377b9b6bca",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}