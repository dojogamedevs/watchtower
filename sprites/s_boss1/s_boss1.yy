{
    "id": "79019127-0eab-4b64-87f6-6c54f5598e5d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_boss1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 44,
    "bbox_left": 0,
    "bbox_right": 47,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "adf773e2-8775-40b0-abde-e302dd7b649c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "79019127-0eab-4b64-87f6-6c54f5598e5d",
            "compositeImage": {
                "id": "7e2c18fa-168f-433d-bf81-8ac701f32f63",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "adf773e2-8775-40b0-abde-e302dd7b649c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bc66eac9-ebad-43a9-a9ca-6b7eb8069f54",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "adf773e2-8775-40b0-abde-e302dd7b649c",
                    "LayerId": "4f421887-1c2d-4517-874d-aa3e00452a9e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "4f421887-1c2d-4517-874d-aa3e00452a9e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "79019127-0eab-4b64-87f6-6c54f5598e5d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}