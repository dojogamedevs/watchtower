{
    "id": "363eb404-57a9-4523-a264-368b3f61be64",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bullet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 1,
    "bbox_right": 15,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a0b7712d-c44f-49ad-bbad-66498300809e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "363eb404-57a9-4523-a264-368b3f61be64",
            "compositeImage": {
                "id": "fd04741d-9e1c-4948-82de-8181b7e2bcf5",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a0b7712d-c44f-49ad-bbad-66498300809e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a833f76e-0f9f-4717-9a6e-d68dfac206b2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a0b7712d-c44f-49ad-bbad-66498300809e",
                    "LayerId": "8b69c680-17ae-478e-82bd-72df643bb316"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "8b69c680-17ae-478e-82bd-72df643bb316",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "363eb404-57a9-4523-a264-368b3f61be64",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 8,
    "yorig": 8
}