{
    "id": "59c0f3bb-3b49-41ec-930f-2da9b48d6371",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_c_ballista",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 0,
    "bbox_right": 89,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f0d3ff83-dbf8-41c6-8c3b-4675b0ea6fae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "59c0f3bb-3b49-41ec-930f-2da9b48d6371",
            "compositeImage": {
                "id": "86b5aeaa-8336-479b-9ecb-54ea6922cfc4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f0d3ff83-dbf8-41c6-8c3b-4675b0ea6fae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffbe804c-6327-4597-bc02-452322cc34b7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f0d3ff83-dbf8-41c6-8c3b-4675b0ea6fae",
                    "LayerId": "a2e30285-285c-4a64-8dad-f0b41f015a6d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 105,
    "layers": [
        {
            "id": "a2e30285-285c-4a64-8dad-f0b41f015a6d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "59c0f3bb-3b49-41ec-930f-2da9b48d6371",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 0,
    "yorig": 0
}