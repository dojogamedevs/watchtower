{
    "id": "e296e041-17b6-4038-b6dc-08903bd62f99",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_c_bolt",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 0,
    "bbox_right": 89,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "57756b67-7632-493d-ab14-5904ce572221",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e296e041-17b6-4038-b6dc-08903bd62f99",
            "compositeImage": {
                "id": "3839b17e-3433-4c8a-bbcc-c24794dbd9fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57756b67-7632-493d-ab14-5904ce572221",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5febe8c7-feff-447d-a85e-50a02aa24419",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57756b67-7632-493d-ab14-5904ce572221",
                    "LayerId": "e1c71946-750e-4c36-89ed-9fc8246de8f7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 105,
    "layers": [
        {
            "id": "e1c71946-750e-4c36-89ed-9fc8246de8f7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e296e041-17b6-4038-b6dc-08903bd62f99",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 0,
    "yorig": 0
}