{
    "id": "98f6edd4-24e5-4037-ae71-7bceb8610b11",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_c_catapult",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 0,
    "bbox_right": 89,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "a32cf504-cd9c-41c4-adce-ad9b85375fd8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "98f6edd4-24e5-4037-ae71-7bceb8610b11",
            "compositeImage": {
                "id": "875fae77-463b-419d-9be8-94a4a8202657",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a32cf504-cd9c-41c4-adce-ad9b85375fd8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cb30ff23-ef48-4613-82ff-5a1be19d85e5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a32cf504-cd9c-41c4-adce-ad9b85375fd8",
                    "LayerId": "bb526bb3-8f5a-4568-87e1-48b9ce1bd65e"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 105,
    "layers": [
        {
            "id": "bb526bb3-8f5a-4568-87e1-48b9ce1bd65e",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "98f6edd4-24e5-4037-ae71-7bceb8610b11",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 0,
    "yorig": 0
}