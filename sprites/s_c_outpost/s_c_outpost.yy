{
    "id": "c77b6449-d179-4149-8c60-207effa2dbd1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_c_outpost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 0,
    "bbox_right": 89,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9ea685ac-8b35-4fe0-9e13-e22e325d8edf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c77b6449-d179-4149-8c60-207effa2dbd1",
            "compositeImage": {
                "id": "c514fc1f-9caf-4124-8078-060cef1894de",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9ea685ac-8b35-4fe0-9e13-e22e325d8edf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b679265-990a-4a1a-8669-14eea2e82788",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9ea685ac-8b35-4fe0-9e13-e22e325d8edf",
                    "LayerId": "f5a5efda-547e-4145-8c69-d6eb879c475b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 105,
    "layers": [
        {
            "id": "f5a5efda-547e-4145-8c69-d6eb879c475b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c77b6449-d179-4149-8c60-207effa2dbd1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 0,
    "yorig": 0
}