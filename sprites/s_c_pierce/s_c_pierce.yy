{
    "id": "402faf73-e25a-4086-914d-3252bd37db03",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_c_pierce",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 0,
    "bbox_right": 89,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4a47b53c-d097-4a13-b4da-8d4b6f7a75de",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "402faf73-e25a-4086-914d-3252bd37db03",
            "compositeImage": {
                "id": "7bcb7a86-7bd4-4e3d-ba5c-856200d6753c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4a47b53c-d097-4a13-b4da-8d4b6f7a75de",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "498c4078-152f-4fcc-876c-c74c52276e9e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4a47b53c-d097-4a13-b4da-8d4b6f7a75de",
                    "LayerId": "4966605a-e5db-4c3a-a2e4-19e307f25400"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 105,
    "layers": [
        {
            "id": "4966605a-e5db-4c3a-a2e4-19e307f25400",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "402faf73-e25a-4086-914d-3252bd37db03",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 0,
    "yorig": 0
}