{
    "id": "428da71b-5816-45e6-9b6d-f6d35cd9cc9d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_c_prep",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 0,
    "bbox_right": 89,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "aa336e06-f5b7-4786-84d7-bf0e801f3141",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "428da71b-5816-45e6-9b6d-f6d35cd9cc9d",
            "compositeImage": {
                "id": "d3eb2179-cb85-4a88-a556-73f0db2ebd57",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aa336e06-f5b7-4786-84d7-bf0e801f3141",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4bd3b72e-e837-4285-b2fa-cccbcd6b7293",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aa336e06-f5b7-4786-84d7-bf0e801f3141",
                    "LayerId": "2a25b6bf-52b0-432c-8b1d-658707bc2eec"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 105,
    "layers": [
        {
            "id": "2a25b6bf-52b0-432c-8b1d-658707bc2eec",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "428da71b-5816-45e6-9b6d-f6d35cd9cc9d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 0,
    "yorig": 0
}