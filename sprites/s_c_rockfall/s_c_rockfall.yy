{
    "id": "636b2577-5959-492f-9e35-f8f81b42b7fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_c_rockfall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 0,
    "bbox_right": 89,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "309e71f3-5f14-4c4f-af2c-3228a2ec476c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "636b2577-5959-492f-9e35-f8f81b42b7fe",
            "compositeImage": {
                "id": "37026f58-c047-491a-a3d8-1a84ae0c06e0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "309e71f3-5f14-4c4f-af2c-3228a2ec476c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f459239f-2d27-4371-8371-e6dadfeed4d6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "309e71f3-5f14-4c4f-af2c-3228a2ec476c",
                    "LayerId": "822ebcf8-22c4-4b21-ac64-f24f3f66a88f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 105,
    "layers": [
        {
            "id": "822ebcf8-22c4-4b21-ac64-f24f3f66a88f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "636b2577-5959-492f-9e35-f8f81b42b7fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 0,
    "yorig": 0
}