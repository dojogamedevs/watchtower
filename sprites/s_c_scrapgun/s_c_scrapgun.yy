{
    "id": "9baaa103-26ac-4c0e-ae01-78539653529f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_c_scrapgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 0,
    "bbox_right": 89,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b5b80315-7595-4fe4-8e86-4dae074be854",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9baaa103-26ac-4c0e-ae01-78539653529f",
            "compositeImage": {
                "id": "d88ca11e-a987-4cbd-81ac-e33c90818390",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b5b80315-7595-4fe4-8e86-4dae074be854",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c228d302-d80e-440f-9227-e8bc894c4395",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b5b80315-7595-4fe4-8e86-4dae074be854",
                    "LayerId": "c74dfb22-c451-4c58-b55f-096625f4a814"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 105,
    "layers": [
        {
            "id": "c74dfb22-c451-4c58-b55f-096625f4a814",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9baaa103-26ac-4c0e-ae01-78539653529f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 0,
    "yorig": 0
}