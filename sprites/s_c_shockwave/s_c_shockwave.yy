{
    "id": "ccd96987-5a4b-4c00-ac06-4b56db76b06b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_c_shockwave",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 0,
    "bbox_right": 89,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ca5ca735-c85d-4c00-a80d-bdcf7cb0079b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ccd96987-5a4b-4c00-ac06-4b56db76b06b",
            "compositeImage": {
                "id": "4be59d83-6c33-4067-8bda-4f374d710c4d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ca5ca735-c85d-4c00-a80d-bdcf7cb0079b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "929a74ef-5398-423f-a714-8e75f2d45cdd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ca5ca735-c85d-4c00-a80d-bdcf7cb0079b",
                    "LayerId": "31678e10-d9e4-48c3-939f-b39759a2900a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 105,
    "layers": [
        {
            "id": "31678e10-d9e4-48c3-939f-b39759a2900a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ccd96987-5a4b-4c00-ac06-4b56db76b06b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 0,
    "yorig": 0
}