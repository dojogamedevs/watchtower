{
    "id": "38384fcb-e875-4e90-9484-bae986ce3c42",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_c_sniper",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 104,
    "bbox_left": 0,
    "bbox_right": 89,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "95decb06-1b09-4303-afcd-ece00ca82380",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "38384fcb-e875-4e90-9484-bae986ce3c42",
            "compositeImage": {
                "id": "fe9a90aa-fcb8-404d-8644-4efaed56773d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "95decb06-1b09-4303-afcd-ece00ca82380",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b13cf0af-5722-4fda-9fd6-7ecd5bb7f3aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "95decb06-1b09-4303-afcd-ece00ca82380",
                    "LayerId": "4815987b-412b-4334-a4c8-e1be7bb1e498"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 105,
    "layers": [
        {
            "id": "4815987b-412b-4334-a4c8-e1be7bb1e498",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "38384fcb-e875-4e90-9484-bae986ce3c42",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 0,
    "yorig": 0
}