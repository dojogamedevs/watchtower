{
    "id": "57e8f8bf-0659-446a-a564-23844d02b65b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_c_sniper1",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 98,
    "bbox_left": 2,
    "bbox_right": 87,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "98b7be05-48c8-4513-9574-f445ac93211e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57e8f8bf-0659-446a-a564-23844d02b65b",
            "compositeImage": {
                "id": "c88e56f4-3fcf-4322-ab37-d2db09e667fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "98b7be05-48c8-4513-9574-f445ac93211e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b77b0ee-cb72-4602-9d4a-e31623887e2a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "98b7be05-48c8-4513-9574-f445ac93211e",
                    "LayerId": "62c08853-f153-4847-af71-0ca2ee1ecc67"
                }
            ]
        },
        {
            "id": "c86063bd-dee7-4c17-9839-5085531971ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57e8f8bf-0659-446a-a564-23844d02b65b",
            "compositeImage": {
                "id": "ea48e50b-b295-4769-8d72-e8816ae28118",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c86063bd-dee7-4c17-9839-5085531971ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8c408c2e-6ea3-4194-8705-a130382baafd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c86063bd-dee7-4c17-9839-5085531971ae",
                    "LayerId": "62c08853-f153-4847-af71-0ca2ee1ecc67"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 105,
    "layers": [
        {
            "id": "62c08853-f153-4847-af71-0ca2ee1ecc67",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57e8f8bf-0659-446a-a564-23844d02b65b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 90,
    "xorig": 0,
    "yorig": 0
}