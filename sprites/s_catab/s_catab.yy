{
    "id": "155b0836-436f-429f-bfbe-f4589372550e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_catab",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 22,
    "bbox_left": 1,
    "bbox_right": 23,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9320fdf5-fb3c-4aa6-b4e5-ea58996c0c35",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "155b0836-436f-429f-bfbe-f4589372550e",
            "compositeImage": {
                "id": "ca88927e-0415-44ad-ac8f-60597d65ec35",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9320fdf5-fb3c-4aa6-b4e5-ea58996c0c35",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8fae4357-bfea-4d45-9d8b-601e18bd017e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9320fdf5-fb3c-4aa6-b4e5-ea58996c0c35",
                    "LayerId": "bfcdd987-ae98-423a-81e5-2251ce71cd88"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 25,
    "layers": [
        {
            "id": "bfcdd987-ae98-423a-81e5-2251ce71cd88",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "155b0836-436f-429f-bfbe-f4589372550e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 25,
    "xorig": 12,
    "yorig": 12
}