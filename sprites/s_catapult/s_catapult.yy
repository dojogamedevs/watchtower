{
    "id": "58c8c2bf-7933-4a7b-8a61-1d651a89d487",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_catapult",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f7f7bd01-3601-421b-8b8e-fb26ac2a105b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "58c8c2bf-7933-4a7b-8a61-1d651a89d487",
            "compositeImage": {
                "id": "712b9e50-d4db-4245-970b-b9f387e30516",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f7f7bd01-3601-421b-8b8e-fb26ac2a105b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3420640d-e471-4bdb-bdbf-3d2e8aa99cdc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f7f7bd01-3601-421b-8b8e-fb26ac2a105b",
                    "LayerId": "b29d64a4-e004-437f-8d06-970186c0d5ee"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "b29d64a4-e004-437f-8d06-970186c0d5ee",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "58c8c2bf-7933-4a7b-8a61-1d651a89d487",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}