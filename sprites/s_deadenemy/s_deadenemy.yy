{
    "id": "bde22c92-93cd-47fc-aae7-6d7d21ef859c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_deadenemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 29,
    "bbox_left": 3,
    "bbox_right": 31,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "906fc167-0bce-45d3-9251-c0c70ab1ea94",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "bde22c92-93cd-47fc-aae7-6d7d21ef859c",
            "compositeImage": {
                "id": "7d08c6b9-2a9c-400b-a346-3cd89063c14b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "906fc167-0bce-45d3-9251-c0c70ab1ea94",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b9563048-bb61-4fc4-9cb8-6c67f0ce8ee6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "906fc167-0bce-45d3-9251-c0c70ab1ea94",
                    "LayerId": "74d422a0-22b1-4f0d-8a1b-cb39079bf2ba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "74d422a0-22b1-4f0d-8a1b-cb39079bf2ba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "bde22c92-93cd-47fc-aae7-6d7d21ef859c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}