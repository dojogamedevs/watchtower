{
    "id": "55388686-9a3e-42ca-8a01-849a8f2c0dff",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_effect_lightning",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 28,
    "bbox_left": 0,
    "bbox_right": 24,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "980eb7d2-81f1-4c2e-bf06-84805e546c1c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55388686-9a3e-42ca-8a01-849a8f2c0dff",
            "compositeImage": {
                "id": "70ad6bcf-6049-406d-84b9-ba6a57560be7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "980eb7d2-81f1-4c2e-bf06-84805e546c1c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "a90959a0-ec47-4346-8af2-84c7d561dcf5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "980eb7d2-81f1-4c2e-bf06-84805e546c1c",
                    "LayerId": "2e349034-583e-4bd4-b6b7-683b5c43d94c"
                }
            ]
        },
        {
            "id": "747afa1d-14ea-4d21-be5d-bd6212ea5fde",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "55388686-9a3e-42ca-8a01-849a8f2c0dff",
            "compositeImage": {
                "id": "6b4aa0cf-cef9-4ab4-900d-401b7ca193ec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "747afa1d-14ea-4d21-be5d-bd6212ea5fde",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3b28b366-5681-4d73-87a9-60d21f041e94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "747afa1d-14ea-4d21-be5d-bd6212ea5fde",
                    "LayerId": "2e349034-583e-4bd4-b6b7-683b5c43d94c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2e349034-583e-4bd4-b6b7-683b5c43d94c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "55388686-9a3e-42ca-8a01-849a8f2c0dff",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}