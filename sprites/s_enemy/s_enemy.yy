{
    "id": "756dc221-bc20-4fcd-95c4-65f99b5ee88f",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_enemy",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c35ad199-fe04-4e79-b80b-35f79810c57d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "756dc221-bc20-4fcd-95c4-65f99b5ee88f",
            "compositeImage": {
                "id": "18c55189-d169-4d52-b81a-7fe66d9e15fc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c35ad199-fe04-4e79-b80b-35f79810c57d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "00ec35ba-101c-4ab1-9290-c9bf07458d50",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c35ad199-fe04-4e79-b80b-35f79810c57d",
                    "LayerId": "2aa89c58-ff8b-4437-82ad-fb3395d7d05a"
                }
            ]
        },
        {
            "id": "de4f8f42-21d2-48f7-a3b9-8de04d82e04e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "756dc221-bc20-4fcd-95c4-65f99b5ee88f",
            "compositeImage": {
                "id": "e65db19d-5240-4d11-a9df-570092eac8ab",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "de4f8f42-21d2-48f7-a3b9-8de04d82e04e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "66482b35-9940-4d52-ab16-4bc9ac8d4aca",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "de4f8f42-21d2-48f7-a3b9-8de04d82e04e",
                    "LayerId": "2aa89c58-ff8b-4437-82ad-fb3395d7d05a"
                }
            ]
        },
        {
            "id": "86b61877-4101-4446-bcbf-b3b94b22ecf0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "756dc221-bc20-4fcd-95c4-65f99b5ee88f",
            "compositeImage": {
                "id": "afd8563c-2b03-44c1-9ea5-d08fd85a5878",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "86b61877-4101-4446-bcbf-b3b94b22ecf0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "b3f0ce83-d5a3-4f67-b2a2-528c52a0c914",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "86b61877-4101-4446-bcbf-b3b94b22ecf0",
                    "LayerId": "2aa89c58-ff8b-4437-82ad-fb3395d7d05a"
                }
            ]
        },
        {
            "id": "57991125-8ce6-46e5-97d6-8ee3cfce5165",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "756dc221-bc20-4fcd-95c4-65f99b5ee88f",
            "compositeImage": {
                "id": "86817267-6864-4aa9-945e-35c157b5d044",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "57991125-8ce6-46e5-97d6-8ee3cfce5165",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13fdab8d-6fe2-45e5-afeb-42a1a9034bfc",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "57991125-8ce6-46e5-97d6-8ee3cfce5165",
                    "LayerId": "2aa89c58-ff8b-4437-82ad-fb3395d7d05a"
                }
            ]
        },
        {
            "id": "c4b25ab1-76d5-4a87-ab83-d05eab4d5e9a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "756dc221-bc20-4fcd-95c4-65f99b5ee88f",
            "compositeImage": {
                "id": "c70b79f3-0a57-45ba-86c0-3a9c5f6217ac",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c4b25ab1-76d5-4a87-ab83-d05eab4d5e9a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3ef0f3e-182d-4f34-8c7c-458a25a91850",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c4b25ab1-76d5-4a87-ab83-d05eab4d5e9a",
                    "LayerId": "2aa89c58-ff8b-4437-82ad-fb3395d7d05a"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2aa89c58-ff8b-4437-82ad-fb3395d7d05a",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "756dc221-bc20-4fcd-95c4-65f99b5ee88f",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": [
        4278190335,
        4278255615,
        4278255360,
        4294967040,
        4294901760,
        4294902015,
        4294967295,
        4293717228,
        4293059298,
        4292335575,
        4291677645,
        4290230199,
        4287993237,
        4280556782,
        4278252287,
        4283540992,
        4293963264,
        4287770926,
        4287365357,
        4287203721,
        4286414205,
        4285558896,
        4284703587,
        4283782485,
        4281742902,
        4278190080,
        4286158839,
        4286688762,
        4287219453,
        4288280831,
        4288405444,
        4288468131,
        4288465538,
        4291349882,
        4294430829,
        4292454269,
        4291466115,
        4290675079,
        4290743485,
        4290943732,
        4288518390,
        4283395315,
        4283862775,
        4284329979,
        4285068799,
        4285781164,
        4285973884,
        4286101564,
        4290034460,
        4294164224,
        4291529796,
        4289289312,
        4289290373,
        4289291432,
        4289359601,
        4286410226,
        4280556782,
        4280444402,
        4280128760,
        4278252287,
        4282369933,
        4283086137,
        4283540992,
        4288522496,
        4293963264,
        4290540032,
        4289423360,
        4289090560,
        4287770926,
        4287704422,
        4287571858,
        4287365357,
        4284159214,
        4279176094,
        4279058848,
        4278870691,
        4278231211,
        4281367321
    ],
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 15,
    "yorig": 19
}