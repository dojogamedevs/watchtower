{
    "id": "36049ee5-b2ca-448e-8216-cb28ce0fa643",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_fallingrocks",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 0,
    "bbox_right": 44,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "e1c512f0-a63b-4c20-ba63-260c38bc4893",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36049ee5-b2ca-448e-8216-cb28ce0fa643",
            "compositeImage": {
                "id": "55f42025-18ac-4d07-bcbb-1481bf1fce88",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e1c512f0-a63b-4c20-ba63-260c38bc4893",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "39911b4d-cd89-4fe3-971f-8daa31524cb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e1c512f0-a63b-4c20-ba63-260c38bc4893",
                    "LayerId": "a921b552-0dd2-4c35-96de-7e2c4859b6c2"
                }
            ]
        },
        {
            "id": "2397f0bc-8a46-4b58-b604-b85537270b28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36049ee5-b2ca-448e-8216-cb28ce0fa643",
            "compositeImage": {
                "id": "7e9e6ea0-5d9d-4f3f-bc90-3e22dac50278",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2397f0bc-8a46-4b58-b604-b85537270b28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e1bc2bdf-b78a-45ac-9c50-3d62b5cb979d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2397f0bc-8a46-4b58-b604-b85537270b28",
                    "LayerId": "a921b552-0dd2-4c35-96de-7e2c4859b6c2"
                }
            ]
        },
        {
            "id": "059671b6-be94-4e30-9904-eaa744831977",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36049ee5-b2ca-448e-8216-cb28ce0fa643",
            "compositeImage": {
                "id": "fd6f63cc-83cb-4ebc-a379-320c5b7f6c73",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "059671b6-be94-4e30-9904-eaa744831977",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "30e62c87-834a-4157-8e34-8d50fcf94875",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "059671b6-be94-4e30-9904-eaa744831977",
                    "LayerId": "a921b552-0dd2-4c35-96de-7e2c4859b6c2"
                }
            ]
        },
        {
            "id": "4897b510-f86c-498f-a2ec-ab247872be8f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36049ee5-b2ca-448e-8216-cb28ce0fa643",
            "compositeImage": {
                "id": "b1f99329-8a9a-4034-adc2-42a6677e5ceb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4897b510-f86c-498f-a2ec-ab247872be8f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c045a408-7a6b-4972-92b1-6650e2d7b2e6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4897b510-f86c-498f-a2ec-ab247872be8f",
                    "LayerId": "a921b552-0dd2-4c35-96de-7e2c4859b6c2"
                }
            ]
        },
        {
            "id": "0100a4b5-3c35-45a4-9c4f-634f6b20a2f4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36049ee5-b2ca-448e-8216-cb28ce0fa643",
            "compositeImage": {
                "id": "2ca5f293-863b-49de-826b-17a692f3785e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0100a4b5-3c35-45a4-9c4f-634f6b20a2f4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "381f0e53-6f98-4404-ac74-52a930982ab5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0100a4b5-3c35-45a4-9c4f-634f6b20a2f4",
                    "LayerId": "a921b552-0dd2-4c35-96de-7e2c4859b6c2"
                }
            ]
        },
        {
            "id": "703513b3-1deb-444b-8f60-6e1d9eed506b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36049ee5-b2ca-448e-8216-cb28ce0fa643",
            "compositeImage": {
                "id": "25f41881-22fe-41e6-81f5-ea2e05faa23e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "703513b3-1deb-444b-8f60-6e1d9eed506b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e0bf11a2-a03f-4ed7-9f43-f3fabaed2be8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "703513b3-1deb-444b-8f60-6e1d9eed506b",
                    "LayerId": "a921b552-0dd2-4c35-96de-7e2c4859b6c2"
                }
            ]
        },
        {
            "id": "6eef2ddf-cc84-4294-b785-b5fd7c0f1789",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36049ee5-b2ca-448e-8216-cb28ce0fa643",
            "compositeImage": {
                "id": "4c193e9d-efb3-42fe-acb8-4fe039fd41d0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6eef2ddf-cc84-4294-b785-b5fd7c0f1789",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "75eba273-bf48-450a-8552-cedfa13d2cb9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6eef2ddf-cc84-4294-b785-b5fd7c0f1789",
                    "LayerId": "a921b552-0dd2-4c35-96de-7e2c4859b6c2"
                }
            ]
        },
        {
            "id": "51dff400-bd64-49ae-9810-a7b297b61984",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36049ee5-b2ca-448e-8216-cb28ce0fa643",
            "compositeImage": {
                "id": "d9e27be4-c7a9-4fb8-987d-262f029b1bcc",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51dff400-bd64-49ae-9810-a7b297b61984",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3033068c-a5bd-41e0-aaa3-aec04b57523a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51dff400-bd64-49ae-9810-a7b297b61984",
                    "LayerId": "a921b552-0dd2-4c35-96de-7e2c4859b6c2"
                }
            ]
        },
        {
            "id": "07362187-efc0-46c3-a632-002ec39aaaa1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36049ee5-b2ca-448e-8216-cb28ce0fa643",
            "compositeImage": {
                "id": "a371bc4e-c915-42e6-a94c-a7a18f3a6d4e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "07362187-efc0-46c3-a632-002ec39aaaa1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c54691b9-71b2-4ccf-a7c7-bcde1622aa31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "07362187-efc0-46c3-a632-002ec39aaaa1",
                    "LayerId": "a921b552-0dd2-4c35-96de-7e2c4859b6c2"
                }
            ]
        },
        {
            "id": "e9ddc4fc-fb9f-4756-8709-6140a9d2d89f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36049ee5-b2ca-448e-8216-cb28ce0fa643",
            "compositeImage": {
                "id": "c7d9dd86-43be-4d8a-a93e-d5909c8b5361",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "e9ddc4fc-fb9f-4756-8709-6140a9d2d89f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2bad3ef2-dad2-4f8e-bd21-5950d4f1ea6e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "e9ddc4fc-fb9f-4756-8709-6140a9d2d89f",
                    "LayerId": "a921b552-0dd2-4c35-96de-7e2c4859b6c2"
                }
            ]
        },
        {
            "id": "89981365-5209-4a1e-8d65-5c41e716d155",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36049ee5-b2ca-448e-8216-cb28ce0fa643",
            "compositeImage": {
                "id": "7462cb62-4f86-4f50-ae62-25cf308a7aba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "89981365-5209-4a1e-8d65-5c41e716d155",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7edec804-4a60-4346-b08d-d95ce5c7ebf1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "89981365-5209-4a1e-8d65-5c41e716d155",
                    "LayerId": "a921b552-0dd2-4c35-96de-7e2c4859b6c2"
                }
            ]
        },
        {
            "id": "34ab7759-57dd-4e7a-bcb0-562cad1f54e7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36049ee5-b2ca-448e-8216-cb28ce0fa643",
            "compositeImage": {
                "id": "89d2e481-af0c-4af9-84b0-4a41dfc3db42",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "34ab7759-57dd-4e7a-bcb0-562cad1f54e7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "481e832e-d7f9-46c7-9753-853047705823",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "34ab7759-57dd-4e7a-bcb0-562cad1f54e7",
                    "LayerId": "a921b552-0dd2-4c35-96de-7e2c4859b6c2"
                }
            ]
        },
        {
            "id": "ed5aec12-6adc-46e9-850b-708de77e8713",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "36049ee5-b2ca-448e-8216-cb28ce0fa643",
            "compositeImage": {
                "id": "ad86c7cb-0147-42ab-bbd5-db1d27104d5d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ed5aec12-6adc-46e9-850b-708de77e8713",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "8577e1ff-eb3a-4f53-99e7-1832b11e2ee5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ed5aec12-6adc-46e9-850b-708de77e8713",
                    "LayerId": "a921b552-0dd2-4c35-96de-7e2c4859b6c2"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 48,
    "layers": [
        {
            "id": "a921b552-0dd2-4c35-96de-7e2c4859b6c2",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "36049ee5-b2ca-448e-8216-cb28ce0fa643",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 60,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}