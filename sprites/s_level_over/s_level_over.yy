{
    "id": "82a427cb-5368-4fa1-ae19-a111e3637a2c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_level_over",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 249,
    "bbox_left": 0,
    "bbox_right": 349,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8c59660f-b48b-48f5-a0a3-7cb9e1228fc7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "82a427cb-5368-4fa1-ae19-a111e3637a2c",
            "compositeImage": {
                "id": "491061ba-33c8-4cf8-9609-444f6ce55754",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8c59660f-b48b-48f5-a0a3-7cb9e1228fc7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e3d5b248-3329-4939-af98-5fd3e79a8e2e",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8c59660f-b48b-48f5-a0a3-7cb9e1228fc7",
                    "LayerId": "df5853ea-3e82-4518-9674-8e7ccf18af2f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 250,
    "layers": [
        {
            "id": "df5853ea-3e82-4518-9674-8e7ccf18af2f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "82a427cb-5368-4fa1-ae19-a111e3637a2c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 350,
    "xorig": 0,
    "yorig": 0
}