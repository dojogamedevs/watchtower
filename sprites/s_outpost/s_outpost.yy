{
    "id": "285d4fd3-863e-4374-8bc6-0596cb96bb40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_outpost",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "67233628-7d22-459b-a080-f07903edef5d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "285d4fd3-863e-4374-8bc6-0596cb96bb40",
            "compositeImage": {
                "id": "23c80526-8044-471f-8bdb-9675688f7ffe",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "67233628-7d22-459b-a080-f07903edef5d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd251ca7-f21b-4fef-a9fb-72f0a2df3ca6",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "67233628-7d22-459b-a080-f07903edef5d",
                    "LayerId": "e6958ee1-412f-4d98-bf72-2d3948e81c23"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e6958ee1-412f-4d98-bf72-2d3948e81c23",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "285d4fd3-863e-4374-8bc6-0596cb96bb40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}