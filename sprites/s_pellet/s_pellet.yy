{
    "id": "26aa48d9-7207-4a55-9f16-80db2f371f40",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_pellet",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 0,
    "bbox_right": 11,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "40093056-bfbd-4585-8f1b-c1edbb5b911a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "26aa48d9-7207-4a55-9f16-80db2f371f40",
            "compositeImage": {
                "id": "8d0ac946-679c-4398-b795-d08ae952c554",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "40093056-bfbd-4585-8f1b-c1edbb5b911a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1e7b497e-0092-42f8-b8b2-6b5ae97f6454",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "40093056-bfbd-4585-8f1b-c1edbb5b911a",
                    "LayerId": "0d921fa5-1023-4197-9584-2ff1a016a86c"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 12,
    "layers": [
        {
            "id": "0d921fa5-1023-4197-9584-2ff1a016a86c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "26aa48d9-7207-4a55-9f16-80db2f371f40",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 12,
    "xorig": 0,
    "yorig": 0
}