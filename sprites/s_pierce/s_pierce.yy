{
    "id": "57229ca2-e335-4ea7-a7a1-901df40f53fe",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_pierce",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8a57cca8-e21e-477c-bc3c-c3e2e433c22a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "57229ca2-e335-4ea7-a7a1-901df40f53fe",
            "compositeImage": {
                "id": "d42ca7ae-2f06-4c03-919b-f3b504e2c4f7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8a57cca8-e21e-477c-bc3c-c3e2e433c22a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "238a7552-fa90-4b23-8c05-06798ebd2839",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8a57cca8-e21e-477c-bc3c-c3e2e433c22a",
                    "LayerId": "e58ed60f-0fbd-437c-af59-34b5e0fc5c9f"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e58ed60f-0fbd-437c-af59-34b5e0fc5c9f",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "57229ca2-e335-4ea7-a7a1-901df40f53fe",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}