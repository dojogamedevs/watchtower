{
    "id": "d9434990-c811-4160-a28b-552f1fc96375",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_pierceb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "00db6644-0ad6-4c6e-8657-59b5c67ca1b7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d9434990-c811-4160-a28b-552f1fc96375",
            "compositeImage": {
                "id": "782896c7-5048-48e3-8970-daf348191609",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "00db6644-0ad6-4c6e-8657-59b5c67ca1b7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "cec7ec0d-0b3a-4a0b-9f1b-28c88a2c0e15",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "00db6644-0ad6-4c6e-8657-59b5c67ca1b7",
                    "LayerId": "51a44aec-9752-426a-ac14-e4dfabb11c1b"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "51a44aec-9752-426a-ac14-e4dfabb11c1b",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d9434990-c811-4160-a28b-552f1fc96375",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 25,
    "yorig": 4
}