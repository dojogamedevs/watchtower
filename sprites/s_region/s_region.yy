{
    "id": "9590e7cb-771d-4487-8e76-bacc46de56eb",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_region",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "fd31fa1d-373d-4afa-8d8c-925a9225dff4",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "9590e7cb-771d-4487-8e76-bacc46de56eb",
            "compositeImage": {
                "id": "26b3a573-9f81-4759-ac63-109ac2ab8b9b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd31fa1d-373d-4afa-8d8c-925a9225dff4",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3dd04f60-d13d-42df-b7e3-45d63ba782f5",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd31fa1d-373d-4afa-8d8c-925a9225dff4",
                    "LayerId": "e9e983f2-0225-4428-b652-2ebddca32175"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "e9e983f2-0225-4428-b652-2ebddca32175",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "9590e7cb-771d-4487-8e76-bacc46de56eb",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}