{
    "id": "a4992fe5-1221-47c8-9a9f-6c2c43e08194",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_rockfall",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "511dedbf-903d-4424-ace4-667b2776d220",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "a4992fe5-1221-47c8-9a9f-6c2c43e08194",
            "compositeImage": {
                "id": "351c8b70-d6b6-4863-bf81-a996b287e028",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "511dedbf-903d-4424-ace4-667b2776d220",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "be0c8704-a15e-4418-a956-73e589cef232",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "511dedbf-903d-4424-ace4-667b2776d220",
                    "LayerId": "be687842-3760-4bfa-9da3-ef43987fb1ff"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "be687842-3760-4bfa-9da3-ef43987fb1ff",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "a4992fe5-1221-47c8-9a9f-6c2c43e08194",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}