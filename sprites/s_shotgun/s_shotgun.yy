{
    "id": "2c84bea8-f205-4f49-89c6-e6a2b2bcbecd",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_shotgun",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c3e0437a-1354-47d7-94e0-9b7652a9bcaa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2c84bea8-f205-4f49-89c6-e6a2b2bcbecd",
            "compositeImage": {
                "id": "398073cf-238d-49a8-89fa-61cb7f43e258",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c3e0437a-1354-47d7-94e0-9b7652a9bcaa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "135a066a-fb41-4f83-b9aa-bb480b2f843d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c3e0437a-1354-47d7-94e0-9b7652a9bcaa",
                    "LayerId": "ba75a177-960e-4748-a694-73991065d251"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "ba75a177-960e-4748-a694-73991065d251",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2c84bea8-f205-4f49-89c6-e6a2b2bcbecd",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}