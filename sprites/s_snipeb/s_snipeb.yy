{
    "id": "1b2bc9e9-2db9-4596-b513-151d84b09433",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_snipeb",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 15,
    "bbox_left": 0,
    "bbox_right": 15,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "39324b5e-b258-450a-b90a-b860aae04760",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1b2bc9e9-2db9-4596-b513-151d84b09433",
            "compositeImage": {
                "id": "bd65e455-7f73-4f30-a990-44232253016f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39324b5e-b258-450a-b90a-b860aae04760",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f8716919-5747-4071-8cd8-d20a5f5f81c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39324b5e-b258-450a-b90a-b860aae04760",
                    "LayerId": "53c6f61e-52be-48e2-be12-e8bf7f4112da"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "53c6f61e-52be-48e2-be12-e8bf7f4112da",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1b2bc9e9-2db9-4596-b513-151d84b09433",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 0,
    "yorig": 0
}