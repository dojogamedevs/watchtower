{
    "id": "fd6b0c75-aee1-4ebb-820d-bf50f3e269bf",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_sniper",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "808e94f1-4261-4928-bc30-2dcf1267cbf8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fd6b0c75-aee1-4ebb-820d-bf50f3e269bf",
            "compositeImage": {
                "id": "da13d70e-9bc6-43b5-9d9a-af1d84e77661",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "808e94f1-4261-4928-bc30-2dcf1267cbf8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1bde4224-aee9-462b-aec7-1a2dc7033b31",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "808e94f1-4261-4928-bc30-2dcf1267cbf8",
                    "LayerId": "890cf6cb-dd46-4bee-93b6-f705c73bc331"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "890cf6cb-dd46-4bee-93b6-f705c73bc331",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fd6b0c75-aee1-4ebb-820d-bf50f3e269bf",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 16,
    "yorig": 16
}