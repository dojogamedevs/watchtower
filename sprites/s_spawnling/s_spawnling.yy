{
    "id": "30bb9cf4-68da-413e-8810-f5b153228665",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_spawnling",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 11,
    "bbox_left": 1,
    "bbox_right": 13,
    "bbox_top": 5,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0029f770-e471-45e7-b0cf-0be49f9a5fc8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30bb9cf4-68da-413e-8810-f5b153228665",
            "compositeImage": {
                "id": "59358208-fb38-4f54-a62f-76c4e3d962f4",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0029f770-e471-45e7-b0cf-0be49f9a5fc8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "13003449-1006-4eaa-a137-ef27ccbf5551",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0029f770-e471-45e7-b0cf-0be49f9a5fc8",
                    "LayerId": "f6b31ce7-7aaa-4d12-8f61-3e10d55fc9d9"
                }
            ]
        },
        {
            "id": "39446bff-4bc6-4c6b-973a-9f31d9543692",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30bb9cf4-68da-413e-8810-f5b153228665",
            "compositeImage": {
                "id": "94b631a5-6525-4ecc-9d37-babd07ac1513",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39446bff-4bc6-4c6b-973a-9f31d9543692",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd3e8871-caaf-49ea-b411-e37c3f248529",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39446bff-4bc6-4c6b-973a-9f31d9543692",
                    "LayerId": "f6b31ce7-7aaa-4d12-8f61-3e10d55fc9d9"
                }
            ]
        },
        {
            "id": "4e70a1e6-e3cd-4efd-beeb-931aecbec500",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30bb9cf4-68da-413e-8810-f5b153228665",
            "compositeImage": {
                "id": "68a5c1f4-718c-4f7f-8ee2-6ace50847cb9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4e70a1e6-e3cd-4efd-beeb-931aecbec500",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "19180dd1-0644-4075-b5f0-7368f18a887a",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4e70a1e6-e3cd-4efd-beeb-931aecbec500",
                    "LayerId": "f6b31ce7-7aaa-4d12-8f61-3e10d55fc9d9"
                }
            ]
        },
        {
            "id": "dd655281-da10-45e2-943b-91d9a1156d6f",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "30bb9cf4-68da-413e-8810-f5b153228665",
            "compositeImage": {
                "id": "c243eb29-e4f9-40a0-90a1-a82b79ccc8e3",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "dd655281-da10-45e2-943b-91d9a1156d6f",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "760fa037-1ace-4b55-abb3-04a08be05843",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "dd655281-da10-45e2-943b-91d9a1156d6f",
                    "LayerId": "f6b31ce7-7aaa-4d12-8f61-3e10d55fc9d9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 16,
    "layers": [
        {
            "id": "f6b31ce7-7aaa-4d12-8f61-3e10d55fc9d9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "30bb9cf4-68da-413e-8810-f5b153228665",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 16,
    "xorig": 7,
    "yorig": 9
}