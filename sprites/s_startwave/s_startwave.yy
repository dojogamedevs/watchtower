{
    "id": "b8aec022-30fe-4993-bc47-9d2ecbab39a8",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_startwave",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 195,
    "bbox_left": 6,
    "bbox_right": 525,
    "bbox_top": 25,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "54619632-99f5-4a7a-9854-78d60ffc9b89",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8aec022-30fe-4993-bc47-9d2ecbab39a8",
            "compositeImage": {
                "id": "cf45f93b-0552-40b5-9e4f-c763912aa1c9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54619632-99f5-4a7a-9854-78d60ffc9b89",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7e56e5e8-607e-4ca0-86ba-42cc9c37c9a7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54619632-99f5-4a7a-9854-78d60ffc9b89",
                    "LayerId": "9896cc6b-1603-4c36-b1c6-53d451f0e6cb"
                }
            ]
        },
        {
            "id": "6d72ffe0-5ffa-4ef7-8810-dce3f5ba6f28",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b8aec022-30fe-4993-bc47-9d2ecbab39a8",
            "compositeImage": {
                "id": "20116996-46bf-41a4-a52b-c7b2b5667ace",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6d72ffe0-5ffa-4ef7-8810-dce3f5ba6f28",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87f33599-c674-4808-9015-4accff56e8c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6d72ffe0-5ffa-4ef7-8810-dce3f5ba6f28",
                    "LayerId": "9896cc6b-1603-4c36-b1c6-53d451f0e6cb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 210,
    "layers": [
        {
            "id": "9896cc6b-1603-4c36-b1c6-53d451f0e6cb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b8aec022-30fe-4993-bc47-9d2ecbab39a8",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 530,
    "xorig": 0,
    "yorig": 0
}