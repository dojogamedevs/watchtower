{
    "id": "4f1fba2d-6f91-4fb1-a0c5-e1656f5f5767",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_tar",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 29,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "0c8d2852-85dd-4635-ab0c-69f0ec83460b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4f1fba2d-6f91-4fb1-a0c5-e1656f5f5767",
            "compositeImage": {
                "id": "1adf0ce4-42f3-438f-87b9-f8ab947d341f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "0c8d2852-85dd-4635-ab0c-69f0ec83460b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "730d6d59-4836-4e0b-9943-a94d81a29548",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "0c8d2852-85dd-4635-ab0c-69f0ec83460b",
                    "LayerId": "9e4b0427-4803-46f7-a74c-809e38831a93"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9e4b0427-4803-46f7-a74c-809e38831a93",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4f1fba2d-6f91-4fb1-a0c5-e1656f5f5767",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 14,
    "yorig": 16
}