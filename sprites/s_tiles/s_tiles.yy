{
    "id": "1bbc2948-43b0-410e-aaf4-e4f7065db682",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_tiles",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 95,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "362f15af-a740-4651-8ada-63278af329ad",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1bbc2948-43b0-410e-aaf4-e4f7065db682",
            "compositeImage": {
                "id": "84989da3-2b7b-4d5a-b8ea-bf8755687eeb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "362f15af-a740-4651-8ada-63278af329ad",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3c655c04-8161-49b6-a00c-06e2ce105473",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "362f15af-a740-4651-8ada-63278af329ad",
                    "LayerId": "8a8cc12f-8c90-481f-a8cc-6c1aff753819"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "8a8cc12f-8c90-481f-a8cc-6c1aff753819",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1bbc2948-43b0-410e-aaf4-e4f7065db682",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 96,
    "xorig": 0,
    "yorig": 0
}