{
    "id": "897f938b-cf26-4422-9868-fe283e04c23e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite31",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c60aaa5-5897-4c9f-9b05-b197ff2772f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "897f938b-cf26-4422-9868-fe283e04c23e",
            "compositeImage": {
                "id": "a200f267-c131-48ce-89b6-e07c4364a8ba",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c60aaa5-5897-4c9f-9b05-b197ff2772f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "88ccc7d5-8d0b-4756-8f17-3cc19e622658",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c60aaa5-5897-4c9f-9b05-b197ff2772f8",
                    "LayerId": "3c2d705a-e4cd-463c-861a-9c397a43b895"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "3c2d705a-e4cd-463c-861a-9c397a43b895",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "897f938b-cf26-4422-9868-fe283e04c23e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 0,
    "yorig": 0
}