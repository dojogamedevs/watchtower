{
    "id": "b0adc5cc-b2c2-433f-9bba-2cbc1ed00816",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "sprite39",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 0,
    "bbox_left": 0,
    "bbox_right": 0,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c433f1b0-6775-4cd7-ab61-8a621788e9a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "b0adc5cc-b2c2-433f-9bba-2cbc1ed00816",
            "compositeImage": {
                "id": "66e62b92-310a-4996-945d-b4175bad0ceb",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c433f1b0-6775-4cd7-ab61-8a621788e9a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd3edfde-096c-4b61-b748-1a54a9fe98f2",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c433f1b0-6775-4cd7-ab61-8a621788e9a9",
                    "LayerId": "ae3dc3a9-d404-473d-8648-a9e513e2fe26"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "ae3dc3a9-d404-473d-8648-a9e513e2fe26",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "b0adc5cc-b2c2-433f-9bba-2cbc1ed00816",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}