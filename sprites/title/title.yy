{
    "id": "ddc9da26-2ad1-4eae-bc51-8610cfa797fa",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "title",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 499,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "c24f2f41-748d-4853-b0d9-8f46fc33c35d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ddc9da26-2ad1-4eae-bc51-8610cfa797fa",
            "compositeImage": {
                "id": "46b859f3-128c-40d7-accc-436683cc554d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c24f2f41-748d-4853-b0d9-8f46fc33c35d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "fb4143fa-0447-4c41-b67d-2bf54b09a748",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c24f2f41-748d-4853-b0d9-8f46fc33c35d",
                    "LayerId": "78d5c67f-946e-4a6b-913f-acc1c0a09fc9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 500,
    "layers": [
        {
            "id": "78d5c67f-946e-4a6b-913f-acc1c0a09fc9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ddc9da26-2ad1-4eae-bc51-8610cfa797fa",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 0,
    "yorig": 0
}